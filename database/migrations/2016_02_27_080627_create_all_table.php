<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 20);
			$table->integer('sort')->default(0);
			$table->boolean('enabled')->default(true);
		});

		Schema::create('zones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 20);
			$table->integer('country_id')->unsigned()->index();
			$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
			$table->integer('sort')->default(0);
			$table->boolean('enabled')->default(true);
		});

		Schema::create('cities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->double('latitude', 15, 8);
			$table->double('longitude', 15, 8);
			$table->string('title', 20);
			$table->integer('zone_id')->unsigned()->index();
			$table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');
			$table->integer('sort')->default(0);
			$table->boolean('enabled');
		});

		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username', 30)->nullable();
			$table->string('name', 30)->nullable();
			$table->string('password', 128)->nullable();
			$table->string('gender', 60)->nullable();
			$table->string('fb_id', 60)->nullable();
			$table->string('email', 60)->unique();
			$table->string('firstName', 20)->nullable();
			$table->string('lastName', 20)->nullable();
			$table->integer('city_id')->unsigned()->index()->nullable();
			$table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
			$table->string('desc', 300)->nullable();
			$table->string('pic', 100);
			$table->string('website', 255)->nullable();
			$table->string('mobile', 45)->nullable();
			$table->dateTime('birthday')->nullable();
			$table->string('address', 100)->nullable();
			$table->dateTime('lastLogin')->nullable();
			$table->boolean('enabled')->default(true);
			$table->rememberToken();
			$table->timestamps();
		});

		Schema::create('items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 20);
			$table->string('tags', 60)->nullable();
			$table->integer('clicks')->default(0);
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('pic1', 100);
			$table->string('pic2', 100)->nullable();
			$table->string('pic3', 100)->nullable();
			$table->string('pic4', 100)->nullable();
			$table->integer('price');
			$table->integer('city_id')->unsigned()->nullable();
			$table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
			$table->string('desc', 300)->nullable();
			$table->integer('status')->default(0);
			$table->integer('sort');
			$table->boolean('enabled');
			$table->timestamps();
			
		});

		Schema::create('evaluates', function(Blueprint $table)
		{
			$table->integer('item_id')->unsigned()->nullable();
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
			$table->integer('role');
			$table->integer('giver_id')->unsigned();
			$table->foreign('giver_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('type');
			$table->string('content', 300)->nullable();
			$table->timestamps();
		});

		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('item_id')->unsigned();
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
			$table->string('content', 300);
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->boolean('enabled')->default(true);
			$table->timestamps();
		});

		Schema::create('tags', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('mode');
			$table->string('title', 20);
			$table->string('pic', 100)->nullable();
			$table->integer('sort')->default(0);
			$table->boolean('enabled')->default(true);
			$table->timestamps();
		});

		Schema::create('rooms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('item_id')->unsigned();
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');   
			$table->integer('user1')->unsigned();
			$table->foreign('user1')->references('id')->on('users')->onDelete('cascade');   
			$table->integer('user2')->unsigned();
			$table->foreign('user2')->references('id')->on('users')->onDelete('cascade');   
		});	

		Schema::create('messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('item_id')->unsigned();
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
			$table->integer('fromUser_id')->unsigned();
			$table->foreign('fromUser_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('toUser_id')->unsigned();
			$table->foreign('toUser_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('content', 300)->nullable();
			$table->integer('mode')->nullable();
			$table->integer('bid')->nullable();
			$table->integer('room_id')->unsigned();
			$table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');   
			$table->boolean('enabled')->default(true);
			$table->timestamps();
		});

		Schema::create('items_has_tags', function(Blueprint $table)
		{
			$table->integer('items_id')->unsigned()->index();
            $table->foreign('items_id')->references('id')->on('items')->onDelete('cascade');
            $table->integer('tags_id')->unsigned()->index();
            $table->foreign('tags_id')->references('id')->on('tags')->onDelete('cascade');
		});

		

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('items_has_tags', function(Blueprint $table)
     	{
        	$table->dropForeign('items_has_tags_items_id_foreign'); 
			$table->dropForeign('items_has_tags_tags_id_foreign'); 
     	});
		Schema::drop('items_has_tags');

		Schema::table('messages', function(Blueprint $table)
     	{
        	$table->dropForeign('messages_item_id_foreign'); 
			$table->dropForeign('messages_fromuser_id_foreign');
			$table->dropForeign('messages_touser_id_foreign'); 
			$table->dropForeign('messages_room_id_foreign'); 
     	});
		Schema::drop('messages');

		Schema::table('rooms', function(Blueprint $table)
     	{
        	$table->dropForeign('rooms_item_id_foreign'); 
			$table->dropForeign('rooms_user1_foreign'); 
			$table->dropForeign('rooms_user2_foreign'); 
     	});
		Schema::drop('rooms');

		Schema::drop('tags');

		Schema::table('comments', function(Blueprint $table)
     	{
        	$table->dropForeign('comments_item_id_foreign'); 
			$table->dropForeign('comments_user_id_foreign');
     	});
		Schema::drop('comments');

		Schema::table('evaluates', function(Blueprint $table)
     	{
        	$table->dropForeign('evaluates_item_id_foreign'); 
			$table->dropForeign('evaluates_giver_id_foreign');
     	});
		Schema::drop('evaluates');

		Schema::table('items', function(Blueprint $table)
     	{
        	$table->dropForeign('items_user_id_foreign'); 
			$table->dropForeign('items_city_id_foreign');
     	});
		Schema::drop('items');

		Schema::table('users', function(Blueprint $table)
     	{ 
			$table->dropForeign('users_city_id_foreign');
     	});
		Schema::drop('users');

		Schema::table('cities', function(Blueprint $table)
     	{ 
			$table->dropForeign('cities_zone_id_foreign');
     	});
		Schema::drop('cities');

		Schema::table('zones', function(Blueprint $table)
     	{ 
			$table->dropForeign('zones_country_id_foreign');
     	});
		Schema::drop('zones');

		Schema::drop('countries');
		
		
	
		
		
		
		
	}

}
