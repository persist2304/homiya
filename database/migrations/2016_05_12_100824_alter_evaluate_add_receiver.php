<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEvaluateAddReceiver extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('evaluates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('receiver_id')->unsigned();
			$table->foreign('receiver_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::drop('evaluates');

		// Schema::table('evaluates', function(Blueprint $table)
		// {

		// 	$table->dropColumn('id');
		// 	$table->dropForeign('evaluates_receiver_id_foreign');
		// 	$table->dropColumn('receiver_id');
		// });	
	}
}
