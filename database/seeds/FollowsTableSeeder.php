<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Follow;
use Faker\Factory as Faker;



class FollowsTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('follows')->delete();

		//WS測試資料
		Follow::create(['user_id'=>1,'fan_id'=>2]);
		Follow::create(['user_id'=>1,'fan_id'=>3]);
		Follow::create(['user_id'=>2,'fan_id'=>1]);
		Follow::create(['user_id'=>2,'fan_id'=>3]);

		$faker = Faker::create(); 
        //以下加入新資料。

        for($i=0;$i<100;$i++)
        {
        	$randNum = rand(1,100);
        	$randNum2 = rand(1,100);
			Follow::create(['user_id'=>$randNum,'fan_id'=>$randNum2]);
		}
	}

}
		
