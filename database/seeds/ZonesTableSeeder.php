<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\Zone;



class ZonesTableSeeder extends Seeder 
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// DB::table('zones')->delete();
		// $taiwan = Country::where('title','台灣')->first();
		// if ($taiwan) {
		// 	$titleArray = ['北北基','桃竹苗','中彰投','雲嘉南','高屏','宜花東','外島'];

		// 	for ($i=0; $i <6 ; $i++) 
		// 	{ 
		// 		Zone::create(['title'=>$titleArray[$i],'sort'=>$i,'country_id'=>$taiwan->id]);	
		// 	}
		// }

		DB::table('zones')->delete();
		//$taiwan = Country::where('title','台灣')->first();
		//if ($taiwan) {
			$titleArray = ['台北市','基隆市','新北市','桃園市','新竹市','新竹縣','苗栗縣','台中市','彰化縣','南投縣','雲林縣','嘉義市','嘉義縣','台南市','高雄市','屏東縣','台東縣','花蓮縣','宜蘭縣','澎湖縣','金門縣','連江縣'];

			for ($i=0; $i <22 ; $i++) 
			{ 
				Zone::create(['title'=>$titleArray[$i],'sort'=>$i,'country_id'=>'1']);	
			}
		//}
			
				
	}
}