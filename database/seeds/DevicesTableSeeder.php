<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Device;



class DevicesTableSeeder extends Seeder 
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('devices')->delete();
		//WS測試用資料
		Device::create(['deviceType'=>'ios','deviceCode'=>'69e364b0-f7f0-11e5-a837-0800200c9a66','salt'=>'a376483']);	
	}
}