<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Track;
use Faker\Factory as Faker;



class TracksTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('tracks')->delete();
		//WS測試資料
		Track::create(['item_id'=>1,'user_id'=>2]);
		Track::create(['item_id'=>1,'user_id'=>3]);
		Track::create(['item_id'=>2,'user_id'=>2]);
		Track::create(['item_id'=>2,'user_id'=>3]);
		Track::create(['item_id'=>3,'user_id'=>1]);
		Track::create(['item_id'=>3,'user_id'=>3]);
		Track::create(['item_id'=>4,'user_id'=>1]);
		Track::create(['item_id'=>4,'user_id'=>3]);
		Track::create(['item_id'=>5,'user_id'=>1]);
		Track::create(['item_id'=>5,'user_id'=>3]);
		$faker = Faker::create(); 
        //以下加入新資料。
        for($i=0;$i<100;$i++)
        {
        	$randNum = rand(1,100);
        	$randNum2 = rand(1,100);
			Track::create(['item_id'=>$randNum,'user_id'=>$randNum2]);
		}
	}
}