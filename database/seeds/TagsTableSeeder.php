<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Tag;
use Faker\Factory as Faker;



class TagsTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$s3lPath = "https://s3.amazonaws.com/homiya/";
		DB::table('tags')->delete();

		Tag::create(['title'=>'買東西','mode'=>'0','enabled'=>'1']);
		Tag::create(['title'=>'賣東西','mode'=>'0','enabled'=>'1']);
		Tag::create(['title'=>'出租','mode'=>'0','enabled'=>'1']);
		Tag::create(['title'=>'贈送','mode'=>'0','enabled'=>'1']);
		Tag::create(['title'=>'隱藏分類','mode'=>'0','enabled'=>'0']);
		Tag::create(['title'=>'床架','mode'=>'1','pic'=>$s3lPath.'1.jpg','enabled'=>'1']);
		Tag::create(['title'=>'沙發','mode'=>'1','pic'=>$s3lPath.'2.jpg','enabled'=>'1']);
		Tag::create(['title'=>'茶几','mode'=>'1','pic'=>$s3lPath.'3.jpg','enabled'=>'1']);
		Tag::create(['title'=>'桌椅','mode'=>'1','pic'=>$s3lPath.'4.jpg','enabled'=>'1']);
		Tag::create(['title'=>'辦公家具','mode'=>'1','pic'=>$s3lPath.'5.jpg','enabled'=>'1']);
		Tag::create(['title'=>'櫥櫃','mode'=>'1','pic'=>$s3lPath.'6.jpg','enabled'=>'1']);
		Tag::create(['title'=>'屏風','mode'=>'1','pic'=>$s3lPath.'7.jpg','enabled'=>'1']);
		Tag::create(['title'=>'仿古家具','mode'=>'1','pic'=>$s3lPath.'8.jpg','enabled'=>'1']);
		Tag::create(['title'=>'整理架','mode'=>'1','pic'=>$s3lPath.'9.jpg','enabled'=>'1']);
		Tag::create(['title'=>'兒童傢俱','mode'=>'1','pic'=>$s3lPath.'10.jpg','enabled'=>'1']);
		Tag::create(['title'=>'戶外家具','mode'=>'1','pic'=>$s3lPath.'11.jpg','enabled'=>'1']);
		Tag::create(['title'=>'年終出清','mode'=>'2','enabled'=>'1']);
		Tag::create(['title'=>'八折特價','mode'=>'2','enabled'=>'1']);

		

		// $faker = Faker::create(); 
  //       //以下加入新資料。
  //       for($i=0;$i<20;$i++)
  //       {
  //       	$randMode = rand(0,1);
		// 	Tag::create(['mode'=>$randMode,'title'=>$faker->word,'pic'=>$faker->url]);
		// }
	}
}