<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Item;
use Faker\Factory as Faker;

class ItemsTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('items')->delete();

		$faker = Faker::create(); 
	 	
	 	$s3lPath = "https://s3.amazonaws.com/homiya/";

	 	//WS測試用資料
	 	Item::create(['title'=>"測試商品",'user_id'=>1,'pic1'=>$s3lPath.'11.jpeg','price'=>100,'city_id'=>1,'desc'=>"此商品為測試商品",'sort'=>0,'enabled'=>'1','clicks'=>0,'status'=>0,'tags'=>',2,6,']);
	 	Item::create(['title'=>"測試商品2",'user_id'=>1,'pic1'=>$s3lPath.'12.jpeg','price'=>100,'city_id'=>1,'desc'=>"此商品為測試商品",'sort'=>4,'enabled'=>'1','clicks'=>0,'status'=>0,'tags'=>',2,6,']);
	 	Item::create(['title'=>"測試商品3",'user_id'=>2,'pic1'=>$s3lPath.'13.jpeg','price'=>100,'city_id'=>1,'desc'=>"此商品為測試商品",'sort'=>3,'enabled'=>'1','clicks'=>0,'status'=>0,'tags'=>',2,6,']);
	 	Item::create(['title'=>"測試商品4",'user_id'=>2,'pic1'=>$s3lPath.'14.jpeg','price'=>100,'city_id'=>1,'desc'=>"此商品為測試商品",'sort'=>6,'enabled'=>'1','clicks'=>0,'status'=>0,'tags'=>',2,6,']);
	 	Item::create(['title'=>"測試商品5",'user_id'=>2,'pic1'=>$s3lPath.'15.jpeg','price'=>100,'city_id'=>1,'desc'=>"此商品為測試商品",'sort'=>5,'enabled'=>'1','clicks'=>0,'status'=>0,'tags'=>',2,6,']);
	 	Item::create(['title'=>"關閉商品",'user_id'=>1,'pic1'=>$s3lPath.'16.jpeg','price'=>100,'city_id'=>1,'desc'=>"此商品為關閉商品",'sort'=>2,'enabled'=>'0','clicks'=>0,'status'=>0,'tags'=>',2,6,']);
	 	Item::create(['title'=>"成交商品",'user_id'=>2,'pic1'=>$s3lPath.'16.jpeg','price'=>100,'city_id'=>1,'desc'=>"此商品為成交商品",'sort'=>8,'enabled'=>'0','clicks'=>0,'status'=>1,'tags'=>',2,6,']);

        //以下加入隨機資料。
        for($i=1;$i<21;$i++)
        {
        	if (rand(0,10) > 2) {
        		$randStatus = 0;
        	}else{
        		$randStatus = 1;
        	}
        	$randCityId = rand(1,22);
        	$randUserId = rand(1,100);
        	$randUser = rand(1,5); //交易類型
        	$randOwn = rand(5,18); //分類.促銷活動
        	//測試只有三張圖片
			// Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>'11.jpeg','pic2'=>'20.jpeg','pic3'=>'15.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'0','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus]);

        	// $image_path = $faker->image($dir = 'public/uploads',$width = 300 , $height = 300);
         //    $image_name = str_replace('public/uploads/' , '' , $image_path);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'11.jpeg','pic2'=>$s3lPath.'20.jpeg','pic3'=>$s3lPath.'15.jpeg','pic4'=>$s3lPath.'13.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'12.jpeg','pic2'=>$s3lPath.'19.jpeg','pic3'=>$s3lPath.'16.jpeg','pic4'=>$s3lPath.'12.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'13.jpeg','pic2'=>$s3lPath.'18.jpeg','pic3'=>$s3lPath.'17.jpeg','pic4'=>$s3lPath.'11.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'14.jpeg','pic2'=>$s3lPath.'17.jpeg','pic3'=>$s3lPath.'18.jpeg','pic4'=>$s3lPath.'20.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'15.jpeg','pic2'=>$s3lPath.'16.jpeg','pic3'=>$s3lPath.'19.jpeg','pic4'=>$s3lPath.'19.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'16.jpeg','pic2'=>$s3lPath.'15.jpeg','pic3'=>$s3lPath.'20.jpeg','pic4'=>$s3lPath.'18.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'17.jpeg','pic2'=>$s3lPath.'14.jpeg','pic3'=>$s3lPath.'14.jpeg','pic4'=>$s3lPath.'17.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'18.jpeg','pic2'=>$s3lPath.'13.jpeg','pic3'=>$s3lPath.'11.jpeg','pic4'=>$s3lPath.'16.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'19.jpeg','pic2'=>$s3lPath.'12.jpeg','pic3'=>$s3lPath.'12.jpeg','pic4'=>$s3lPath.'15.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
			Item::create(['title'=>$faker->word,'user_id'=>$i,'pic1'=>$s3lPath.'20.jpeg','pic2'=>$s3lPath.'11.jpeg','pic3'=>$s3lPath.'13.jpeg','pic4'=>$s3lPath.'14.jpeg','price'=>$faker->numberBetween($min = 10000 , $max = 99999),'city_id'=>$randCityId,'desc'=>$faker->word,'sort'=>$i,'enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999),'status'=>$randStatus,'tags'=>','.$randUser.','.$randOwn.',']);
		}
	}

}

	
		
