<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Country;
use Faker\Factory as Faker;



class CountriesTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('countries')->delete();
		Country::create(['title'=>'台灣','sort'=>'0']);		
	}
}
