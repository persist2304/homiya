<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Faker\Factory as Faker;



class UsersTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();


        //建立測試帳號
        User::create(['username'=>“測試號”,'name'=>“測試號”,'password'=>Hash::make('123456'),'gender'=>'male','email'=>"1@gmail.com",'firstName'=>"林",'lastName'=>"測試號",'mobile'=>"0911234567",'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>"1284213091592481",'desc'=>"Hello,World",'pic'=>$localPath.'1.jpeg','enabled'=>'1']);
        User::create(['username'=>“測試號2”,'name'=>“測試號2”,'password'=>Hash::make('123456'),'gender'=>'male','email'=>"2@gmail.com",'firstName'=>"林",'lastName'=>"測試號2",'mobile'=>"0911234568",'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>"1284213091592482",'desc'=>"Hello,World2",'pic'=>$localPath.'2.jpeg','enabled'=>'1']);
        User::create(['username'=>“測試買家”,'name'=>“測試買家”,'password'=>Hash::make('123456'),'gender'=>'female','email'=>"3@gmail.com",'firstName'=>"林",'lastName'=>"測試號3",'mobile'=>"0911234569",'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>"1284213091592483",'desc'=>"Hello,World3",'pic'=>$localPath.'3.jpeg','enabled'=>'1']);

  		$faker = Faker::create(); 
        //以下加入新資料。
        for($i=0;$i<20;$i++)
        {
            $randSex = rand(0,1);
            $localPath = "https://s3.amazonaws.com/homiya/";
        	if($randSex == 0)
         {
             // $image_path = $faker->image($dir = 'public/uploads',$width = 100 , $height = 100);
             // $image_name = str_replace('public/uploads/' , '' , $image_path);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'female','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'1.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'female','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'2.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'female','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'3.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'female','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'4.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'female','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'5.jpeg','enabled'=>'1','address'=>$faker->address]);
         }
         else
         {
             // $image_path = $faker->image($dir = 'public/uploads',$width = 100 , $height = 100);
             // $image_name = str_replace('public/uploads/' , '' , $image_path);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'male','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'6.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'male','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'7.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'male','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'8.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'male','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'9.jpeg','enabled'=>'1','address'=>$faker->address]);
             User::create(['username'=>$faker->userName,'name'=>$faker->name,'password'=>Hash::make('passw0rd'),'gender'=>'male','email'=>$faker->email,'firstName'=>$faker->firstNameMale,'lastName'=>$faker->lastName,'mobile'=>$faker->phoneNumber,'lastLogin'=>$faker->dateTimeThisMonth($max = 'now') ,'fb_id'=>$faker->postcode,'desc'=>$faker->realText($maxNbChars = 200, $indexSize = 2),'birthday'=>$faker->dateTime,'website'=>$faker->url,'pic'=>$localPath.'10.jpeg','enabled'=>'1','address'=>$faker->address]);
         }
        }
    }
}        