<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Banner;
use Faker\Factory as Faker;



class BannersTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('banners')->delete();

		//假資料建立
		$faker = Faker::create();
		$s3lPath = "https://s3.amazonaws.com/homiya/";
		for ($i = 0;$i<1;$i++)
		{
			Banner::create(['title'=>'按摩俏沙發', 'sort'=>1 ,'pic'=>$s3lPath.'sofa_banner.jpg','mode'=>'1','target'=>'6','enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999)]);
			Banner::create(['title'=>'免運專區', 'sort'=>2,'pic'=>$s3lPath.'freeShipping_banner.jpg','mode'=>'2','target'=>'3','enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999)]);
			Banner::create(['title'=>'任賢齊', 'sort'=>4,'pic'=>$s3lPath.'user_banner.jpg','mode'=>'3','target'=>'10','enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999)]);
			Banner::create(['title'=>'最新商品', 'sort'=>3,'pic'=>$s3lPath.'product_banner.jpeg','mode'=>'2','target'=>'10','enabled'=>'1','clicks'=>$faker->numberBetween($min = 10000 , $max = 99999)]);
		}
		
	}
}