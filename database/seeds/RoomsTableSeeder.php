<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Room;



class RoomsTableSeeder extends Seeder 
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('rooms')->delete();
		Room::create(['item_id'=>3,'user1'=>2,'user2'=>1]);
		Room::create(['item_id'=>4,'user1'=>2,'user2'=>1]);
		Room::create(['item_id'=>5,'user1'=>2,'user2'=>1]);
		Room::create(['item_id'=>1,'user1'=>1,'user2'=>2]);
		Room::create(['item_id'=>2,'user1'=>1,'user2'=>2]);
		Room::create(['item_id'=>1,'user1'=>1,'user2'=>3]);
		Room::create(['item_id'=>7,'user1'=>2,'user2'=>1]);
				
	}
}