<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Evaluate;
use Faker\Factory as Faker;



class EvaluatesTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('evaluates')->delete();

		//測試資料
		Evaluate::create(['item_id'=>7,'role'=>2,'giver_id'=>1,'type'=>1,'content'=>"東西很棒"]);
		Evaluate::create(['item_id'=>7,'role'=>1,'giver_id'=>2,'type'=>1,'content'=>"很好的買家"]);

		$faker = Faker::create(); 

        //以下加入新資料。
        for($i=0;$i<100;$i++)
        {
       		$randRole = rand(1,2);
			$randType = rand(1,3);
			$randGiverId = rand(1,100);
			Evaluate::create(['item_id'=>$i,'role'=>$randRole,'giver_id'=>$randGiverId,'type'=>$randType,'content'=>$faker->word]);
		}
	}

}
		
