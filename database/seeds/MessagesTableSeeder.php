<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Message;
use Faker\Factory as Faker;
use Carbon\Carbon;


class MessagesTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('messages')->delete();

		//WS測試資料
		Message::create(['item_id'=>3,'room_id'=>1,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"我有興趣",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 30, 0, null)]);
		Message::create(['item_id'=>3,'room_id'=>1,'fromUser_id'=>'2','toUser_id'=>'1','content'=>"你想出多少",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 32, 0, null)]);
		Message::create(['item_id'=>3,'room_id'=>1,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"出價",'mode'=>1,'bid'=>200,'created_at'=>Carbon::create($null, 4, 1, 12, 35, 0, null)]);
		Message::create(['item_id'=>3,'room_id'=>1,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"出價",'mode'=>1,'bid'=>300,'created_at'=>Carbon::create($null, 4, 1, 12, 37, 0, null)]);
		Message::create(['item_id'=>3,'room_id'=>1,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"出價",'mode'=>1,'bid'=>500,'created_at'=>Carbon::create($null, 4, 1, 12, 39, 0, null)]);
		Message::create(['item_id'=>3,'room_id'=>1,'fromUser_id'=>'1','toUser_id'=>'2','mode'=>2,'created_at'=>Carbon::create($null, 4, 1, 12, 43, 0, null)]);
		Message::create(['item_id'=>3,'room_id'=>1,'fromUser_id'=>'1','toUser_id'=>'2','mode'=>1,'bid'=>800,'created_at'=>Carbon::create($null, 4, 1, 12, 47, 0, null)]);

		Message::create(['item_id'=>4,'room_id'=>2,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"我有興趣2",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 30, 0, null)]);
		Message::create(['item_id'=>4,'room_id'=>2,'fromUser_id'=>'2','toUser_id'=>'1','content'=>"你想出多少2",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 32, 0, null)]);
		Message::create(['item_id'=>4,'room_id'=>2,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"出價",'mode'=>1,'bid'=>500,'created_at'=>Carbon::create($null, 4, 1, 12, 35, 0, null)]);

		Message::create(['item_id'=>5,'room_id'=>3,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"我有興趣3",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 30, 0, null)]);
		Message::create(['item_id'=>5,'room_id'=>3,'fromUser_id'=>'2','toUser_id'=>'1','content'=>"你想出多少3",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 32, 0, null)]);
		Message::create(['item_id'=>5,'room_id'=>3,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"出價",'mode'=>1,'bid'=>600,'created_at'=>Carbon::create($null, 4, 1, 12, 35, 0, null)]);

		Message::create(['item_id'=>1,'room_id'=>4,'fromUser_id'=>'2','toUser_id'=>'1','content'=>"我有興趣4",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 30, 0, null)]);
		Message::create(['item_id'=>1,'room_id'=>4,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"你想出多少4",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 12, 32, 0, null)]);
		Message::create(['item_id'=>1,'room_id'=>4,'fromUser_id'=>'2','toUser_id'=>'1','content'=>"出價",'mode'=>1,'bid'=>700,'created_at'=>Carbon::create($null, 4, 1, 12, 35, 0, null)]);

		Message::create(['item_id'=>2,'room_id'=>5,'fromUser_id'=>'2','toUser_id'=>'1','content'=>"我有興趣5",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 13, 30, 0, null)]);
		Message::create(['item_id'=>2,'room_id'=>5,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"你想出多少5",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 13, 32, 0, null)]);
		Message::create(['item_id'=>2,'room_id'=>5,'fromUser_id'=>'2','toUser_id'=>'1','content'=>"出價",'mode'=>1,'bid'=>800,'created_at'=>Carbon::create($null, 4, 1, 13, 35, 0, null)]);

		Message::create(['item_id'=>1,'room_id'=>6,'fromUser_id'=>'3','toUser_id'=>'1','content'=>"出價",'mode'=>1,'bid'=>600,'created_at'=>Carbon::create($null, 4, 1, 10, 32, 0, null)]);
		Message::create(['item_id'=>1,'room_id'=>6,'fromUser_id'=>'1','toUser_id'=>'3','content'=>"不夠喔",'mode'=>0,'created_at'=>Carbon::create($null, 4, 1, 10, 35, 0, null)]);
		Message::create(['item_id'=>1,'room_id'=>6,'fromUser_id'=>'3','toUser_id'=>'1','content'=>"出價",'mode'=>1,'bid'=>700,'created_at'=>Carbon::create($null, 4, 1, 10, 38, 0, null)]);

		Message::create(['item_id'=>7,'room_id'=>7,'fromUser_id'=>'1','toUser_id'=>'2','content'=>"出價",'mode'=>1,'bid'=>900,'created_at'=>Carbon::create($null, 4, 1, 16, 32, 0, null)]);
		Message::create(['item_id'=>7,'room_id'=>7,'fromUser_id'=>'2','toUser_id'=>'1','mode'=>3,'created_at'=>Carbon::create($null, 4, 1, 16, 35, 0, null)]);
	}
}

