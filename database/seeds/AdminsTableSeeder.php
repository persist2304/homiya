<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Admin;


class AdminsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('admins')->delete();
		Admin::create(['username'=>'admin', 'password'=> bcrypt('passw0rd')]);
		Admin::create(['username'=>'test', 'password'=> bcrypt('passw0rd')]);
	}

}
