<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Comment;
use Faker\Factory as Faker;



class CommentsTableSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('comments')->delete();

		//測試資料
		Comment::create(['item_id'=>1,'content'=>"測試評輪資料",'user_id'=>2]);
		Comment::create(['item_id'=>1,'content'=>"回覆測試評輪資料",'user_id'=>1]);
		Comment::create(['item_id'=>2,'content'=>"測試評輪資料2",'user_id'=>3]);
		Comment::create(['item_id'=>2,'content'=>"回覆測試評輪資料2",'user_id'=>1]);

		Comment::create(['item_id'=>3,'content'=>"測試評輪資料3",'user_id'=>1]);
		Comment::create(['item_id'=>3,'content'=>"測試評輪資料4",'user_id'=>3]);

		$faker = Faker::create(); 
        //以下加入新資料。
        for($i=1;$i<100;$i++)
        {
			Comment::create(['item_id'=>rand(8,100),'content'=>$faker->word,'user_id'=>rand(4,100)]);
		}
	}

}
