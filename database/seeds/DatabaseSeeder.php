<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		if( App::environment() === 'production' )
        {
            exit('請勿在正式環境使用Seed!!');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //清除所有uploads的圖片
        $files = File::files('public/uploads');
        foreach ($files as $value) 
        {
        	File::delete($value);
        }

  //       $this->call('DevicesTableSeeder');
		// $this->command->info('Devices Table Seed Done!');

		// $this->call('BannersTableSeeder');
		// $this->command->info('Banners Table Seed Done!');

		

		// $this->call('CommentsTableSeeder');
		// $this->command->info('Comments Table Seed Done!');

		// $this->call('EvaluatesTableSeeder');
		// $this->command->info('Evaluates Table Seed Done!');

		// $this->call('FollowsTableSeeder');
		// $this->command->info('Follows Table Seed Done!');
		
		// $this->call('CountriesTableSeeder');
		// $this->command->info('Contries Table Seed Done!');

		// $this->call('RoomsTableSeeder');
		// $this->command->info('Rooms Table Seed Done!');
		
		// $this->call('MessagesTableSeeder');
		// $this->command->info('Messages Table Seed Done!');
		
		// $this->call('TagsTableSeeder');
		// $this->command->info('Tags Table Seed Done!');

		// $this->call('TracksTableSeeder');
		// $this->command->info('Tracks Table Seed Done!');
		
		// $this->call('ZonesTableSeeder');
		// $this->command->info('Zones Table Seed Done!');

		// $this->call('CitiesTableSeeder');
		// $this->command->info('Cities Table Seed Done!');

		$this->call('AdminsTableSeeder');
		$this->command->info('Admins Table Seed Done!');

		// $this->call('UsersTableSeeder');
		// $this->command->info('Users Table Seed Done!');

		// $this->call('ItemsTableSeeder');
		// $this->command->info('Items Table Seed Done!');
		
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		
		Model::reguard();
	}

}