@extends('layouts.base')
@section('page_heading','建立新標籤')
@section('section')
<div class="col-sm-12">
{{ Form::open(['action'=>'TagsController@store','role'=>'form','files'=>true]) }}
	@include('tags._form',['submitBtnText'=>'建立'])
{{ Form::close() }}
</div>          
@stop
