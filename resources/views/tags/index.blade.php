@extends('layouts.base')
@section('page_heading','標籤列表')
@section('section')
    @include('vendor.flash.message')

    {{ Form::open(['url'=>'tags/create' , 'method'=>'GET']) }}
    {{ Form::submit('新增標籤',['class'=>'btn btn-info ']) }}
    {{ Form::close() }}
    
    <br/>
    <br/>

    <table id="tb_tags" class="table table-bordered display">
    <thead>
        <tr>
            <th>標籤排序</th>
            <th>標籤名稱</th>
            <th>標籤圖片</th>
            <th>開啟模式</th>
            <th>是否開啟</th>
            <th>創造時間</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tags as $tag)
            <tr>
                <td> {{ $tag->sort }}</td>
                <td><a href="{{ url('tags/' . $tag->id . '/edit') }} ">{{ $tag->title }}</a></td>
                <!--判斷是否有圖片-->
                @if (isset($tag->pic) && ($tag->pic != $isNull))
                    <td><img src="{{$tag->pic}}"/></td>
                @else
                    <td>沒有圖片</td>
                    <!--<td>{{ Html::image(url('demo/' . $tag->pic)) }}</td>--> 
                @endif
                <!--開啟模式-->
                @if ($tag->mode == 0)
                    <td>交易行為</td> 
                @elseif ($tag->mode == 2)
                    <td>促銷活動</td> 
                @else
                    <td>類型</td>
                @endif
                <!--是否開啟-->
                @if ($tag->enabled == 1)
                    <td>是</td>
                @else
                    <td>否</td>
                @endif
                <td>{{ $tag->created_at->format('Y/m/d h:i:s') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@stop

@section('footer')
      <script type="text/javascript">
    $(document).ready(function() 
    {
        var table = $('#tb_tags').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(2)'
            // },
            responsive: true,
        });
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop