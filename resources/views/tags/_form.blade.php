@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6"> 
	    <!-- 標頭title-->
    	@if (isset($errors) and $errors->has('title'))
	    	<div class="form-group has-error">
	    		{{ Form::label('title','標籤名稱') }}&nbsp;&nbsp;{{ Form::label('title',$errors->first('title'),['class'=>'control-label','for'=>'inputError']) }}
				{{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
				<p class="help-block">請輸入需要標籤名稱，限定50個字元</p>
			</div>
    	@else
    		<div class="form-group">
	    		{{ Form::label('title','標籤名稱') }}
				{{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
				<p class="help-block">請輸入需要標籤名稱，限定50個字元</p>
			</div>
    	@endif
    	<!--標籤類型mode-->
    		<div class="form-group" onChange="showOption()";>
    			{{ Form::label('mode','標籤類型') }}
	    		{{ Form::select('mode', ['0'=>'交易行為','1'=>'類型','2'=>'促銷活動','3'=>'細項分類'],null,['class'=>'form-control','rows'=>'4']) }}
			</div>
		<!--加入標籤圖片，在edit呈現-->
        @if (isset($tag) && $urlNow == url ('tags/'.$tag->id.'/edit'))
            <div class="form-group">
            @if (isset($tag->pic))
                {{ Form::label('pic','原本標籤圖片') }}
            @endif
            </div>
            <div>
                @if (isset($tag->pic))
                    <td><img src="{{$tag->pic}}"/></td>
                @else
               		<!--<td>{{ Html::image(url('demo/' . $tag->pic)) }}</td>-->
               		<td></td>                    
                @endif
            </div>
        @else
        @endif   
		<!-- 廣告圖片pic-->
        @if (isset($errors) and $errors->has('pic'))
            <div class="form-group has-error" id = "pic" name = "target_pic">
                {{ Form::label('pic','標籤圖片(建議尺寸:640*360 pixels)') }}&nbsp;&nbsp;{{ Form::label('pic',$errors->first('pic'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::file('pic',null,['class'=>'form-control']) }}
            </div>
        @else
            <div class="form-group" id = "pic" name = "target_pic">
                {{ Form::label('pic','標籤圖片(建議尺寸:640*360 pixels)') }}
                {{ Form::file('pic',null ,['class'=>'form-control']) }}
            </div>
        @endif
<script type="text/javascript">
	//當Page載入完成
	$(document).ready(function()
	{
		hideAll();
		showOption();
	});
	//隱藏所有的target選項
	function hideAll()
	{
		var target_pic = document.getElementById("pic");
		target_pic.style.display="none";
	}
	//顯示與Mode關聯的單位
	function showOption()
	{
		var value = document.getElementById("mode").value;
		hideAll();
		var target;
		if ( value == 0 || value == 2 || value == 3)
		{
			display:none;
		}
		else if ( value == 1)
		{
			target = document.getElementById("pic");
		}
		else
		{
			alert("Mode選項異常");
		}
		target.style.display="inline";
	}
</script>
		<!-- 排序 sort-->
		@if (isset($errors) and $errors->has('sort'))
	    	<div class="form-group has-error">
	    		{{ Form::label('sort','標籤排序(越小越前面)') }}&nbsp;&nbsp;{{ Form::label('sort',$errors->first('sort'),['class'=>'control-label','for'=>'inputError']) }}
				@if (isset($tag))
					{{ Form::text('sort',$tag->sort,['class'=>'form-control']) }}
	    		@else
	    			{{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
	    		@endif
			</div>
    	@else
    		<div class="form-group">
	    		{{ Form::label('sort','標籤排序(越小越前面)') }}
	    		@if (isset($tag))
					{{ Form::text('sort',$tag->sort,['class'=>'form-control']) }}
	    		@else
	    			{{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
	    		@endif
			</div>
    	@endif
		
		<div class="form-group">
			{{ Form::label('enabled','是否啟用') }}
				{{ Form::label('enabled','是',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '1',true) }}
				{{ Form::label('enabled','否',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '0') }}
		</div>

		<div class="form-group">
			{{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
			{{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
		</div>

    </div>

  
</div>