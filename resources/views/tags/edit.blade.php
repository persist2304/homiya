@extends('layouts.base')
@section('page_heading','修改標籤：'.$tag->title)
@section('section')
<div class="col-sm-12">
{{ Form::model($tag,['method'=>'patch','url'=>'tags/'.$tag->id ,'role'=>'form', 'files'=> true]) }}
  @include('tags._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
