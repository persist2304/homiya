@extends('layouts.base')
@section('page_heading','新增文章')
@section('section')
<div class="col-sm-12">
{{ Form::open(['action'=>'ArticleController@store','role'=>'form','files'=>true]) }}
<div class="form-group">
@include('articles._form',['submitBtnText'=>'建立'])
</div>
</div>   
 {{Form::close()}}
@stop
