@extends('layouts.base')
@section('page_heading','新增文章')

@section('section')
    @include('vendor.flash.message')
    {{ Form::open(['url'=>'articles/create' , 'method'=>'GET']) }}
    {{ Form::submit('新增文章',['class'=>'btn btn-info ']) }}
    {{ Form::close() }}
    <br/>

    <table id="tb_article" class="table table-bordered display">
    <thead>
        <tr>
            <th>標題</th>
            <th>創建時間</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($allArticle as $article)
            <tr>
                <td><a href="{{ url('articles/' . $article->id . '/edit') }} ">{{ $article->title }}</a></td>
                <td>{{ $article->created_at->format('Y/m/d H:i:s') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('footer')
    <script type="text/javascript">
    $(document).ready(function() 
    {
        var table = $('#tb_article').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "aaSorting": [1,'asc'],
            rowReorder: 
            {
                selector: 'td:nth-child(2)'
            },
            responsive: true ,
        });
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop