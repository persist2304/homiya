@include('vendor.flash.message')

<!--校園公告標題-->
        @if (isset($errors) and $errors->has('title'))
            <div class="form-group has-error" id = 'campus_announcement_title' >
                {{ Form::label('title','請輸入標題') }}&nbsp;&nbsp;{{ Form::label('title',$errors->first('title'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入標題</p>
            </div>
        @else
            <div class="form-group" id = 'campus_announcement_title'>
                {{ Form::label('title','請輸入標題') }}
                {{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入標題</p>
            </div>
        @endif
          
      
                 <!--開啟模式mode-->
           <div class="form-group" onChange="showOption()";>
                {{ Form::label('mode','開啟模式') }}
                {{ Form::select('mode', ['0'=>'輸入內容','1'=>'輸入超連結'],null,['class'=>'form-control','rows'=>'3']) }}
           </div>
        <!--校園公告內容-->
                @if (isset($errors) and $errors->has('content'))
                    <div class="form-group has-error" id = 'campus_announcement_content' >
                        {{ Form::label('content','請輸入內容') }}&nbsp;&nbsp;{{ Form::label('content',$errors->first('content'),['class'=>'control-label','for'=>'inputError']) }}
                        
                        {{ Form::textarea('content', null, array('class' => 'form-control', 'id' => 'summernote')) }}
                        <p class="help-block">請輸入內容</p>
                    </div>
                @else
                    <div class="form-group" id = 'campus_announcement_content' >
                        {{ Form::label('content','請輸入內容') }}
                        
                        {{ Form::textarea('content', null, array('class' => 'form-control', 'id' => 'summernote')) }}
                        <p class="help-block">請輸入內容</p>
                    </div>
                @endif
        <!--校園公告超連結-->
                @if (isset($errors) and $errors->has('url'))
                    <div class="form-group has-error" id = 'campus_announcement_url' >
                        {{ Form::label('url','請輸入超連結') }}&nbsp;&nbsp;{{ Form::label('url',$errors->first('url'),['class'=>'control-label','for'=>'inputError']) }}
                        {{ Form::text('url',null,['class'=>'form-control','rows'=>'3']) }}
                        
                        <p class="help-block">請輸入超連結</p>
                    </div>
                @else
                    <div class="form-group" id = 'campus_announcement_url' >
                        {{ Form::label('url','請輸入超連結') }}
                        
                        {{ Form::text('url',null,['class'=>'form-control','rows'=>'3']) }}
                        <p class="help-block">請輸入超連結</p>
                    </div>
                @endif

      

      
            <script type="text/javascript">
                    //Page載入完成...
                    $(document).ready(function(){
                        hideAll();
                        showOption();
                        // document.getElementById("url").style.display="inline";
                    });

                    //隱藏所有target選項
                    function hideAll()
                    {
                        var campus_announcement_content = document.getElementById("campus_announcement_content");
                        campus_announcement_content.style.display="none";
                        var campus_announcement_url = document.getElementById("campus_announcement_url");
                        campus_announcement_url.style.display="none";
                    }
                    //顯示與Mode選項關聯的欄位
                    function showOption()
                    {
                        var value = document.getElementById("mode").value;
                        hideAll();
                        var target;
                        if( value == 0 )
                        {
                            //選取網頁
                            target = document.getElementById("campus_announcement_content");
                        }
                        else if( value == 1 )
                        {
                            //選取標籤
                            target = document.getElementById("campus_announcement_url");
                        }
                        else
                        {
                            alert("Mode選項異常");
                        }
                        target.style.display="inline";
                    }
            </script>
      
            
        <!--校園公告按鈕-->
                <div class="form-group">
                    {{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
                </div>
           
     