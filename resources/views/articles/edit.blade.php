@extends('layouts.base')
@section('page_heading','修改')
@section('section')
<div class="col-sm-12">
{{ Form::model($articles_edit,['method'=>'patch','url'=>'articles/'.$articles_edit->id ,'role'=>'form']) }}
  @include('articles._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
@stop
