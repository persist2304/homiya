@extends('layouts.base')
@section('page_heading','修改商品：'.$item->title)
@section('section')
<div class="col-sm-12">
{{ Form::model($item,['method'=>'patch','url'=>'items/'.$item->id ,'role'=>'form', 'files'=> true]) }}
  @include('items._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
