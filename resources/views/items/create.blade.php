@extends('layouts.base')
@section('page_heading','建立新商品')
@section('section')
<div class="col-sm-12">
{{ Form::open(['action'=>'ItemsController@store','role'=>'form','files'=>true]) }}
	@include('items._form',['submitBtnText'=>'建立'])
{{ Form::close() }}
</div>          
           
            
@stop
