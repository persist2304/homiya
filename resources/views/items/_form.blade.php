@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6">
     <!--商品名稱-->
        @if (isset($errors) and $errors->has('title'))
            <div class="form-group has-error">
                {{ Form::label('title','商品名稱') }}&nbsp;&nbsp;{{ Form::label('title',$errors->first('title'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入商品名稱</p>
            </div>
        @else
            <div class="form-group">
                {{ Form::label('title','商品名稱') }}
                {{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入商品名稱</p>
            </div>
        @endif
    <div class="col-lg-6">
        <!--加入商品圖片1，在edit呈現-->
        @if (isset($item) && $urlNow == url ('items/'.$item->id.'/edit'))
            <div class="form-group">
                {{ Form::label('pic1','商品圖片1') }}
            </div>
            <div>  
                @if (isset($item->pic1))
                    <!-- <iframe src="{{$item->pic1}}" style="border:none;" height={{"$high"}} width={{"$width"}}></iframe> -->
                    <td><img src="{{$item->pic1}}" height={{"$high"}} width={{"$width"}}> </img></td>
                @else
                    <td></td>    
                @endif
            </div>
        @else
        @endif
         <!--加入商品圖片3，在edit呈現-->
        @if (isset($item->pic3) && $urlNow == url ('items/'.$item->id.'/edit'))
            <div class="form-group">
                {{ Form::label('pic3','商品圖片3') }}
            </div>
            <div>  
                @if (isset($item->pic3))
                    <!-- <iframe src="{{$item->pic3}}" style="border:none;" height={{"$high"}} width={{"$width3"}}></iframe> -->
                    <img src="{{$item->pic3}}" height={{"$high"}} width={{"$width"}}> </img>
                @else
                    <!--<td>{{ Html::image(url('uploads/' . $item->pic3)) }}</td>  -->
                    <td></td>
                @endif
            </div>
        @else
        @endif

        
    </div>
    <div class="col-lg-6">
       
        <!--加入商品圖片2，在edit呈現-->
        @if (isset($item->pic2) && $urlNow == url ('items/'.$item->id.'/edit'))
            <div class="form-group">
                {{ Form::label('pic2','商品圖片2') }}
            </div>
            <div>  
                @if (isset($item->pic2))
                    <!-- <iframe src="{{$item->pic2}}" style="border:none;" height={{"$high"}} width={{"$width"}}></iframe> -->
                    <img src="{{$item->pic2}}" height={{"$high"}} width={{"$width"}}> </img>
                @else
                    <!--<td>{{ Html::image(url('uploads/' . $item->pic2)) }}</td>-->
                    <td></td>
                @endif
            </div>
        @else
        @endif
        <!--加入商品圖片4，在edit呈現-->
        @if (isset($item->pic4) && $urlNow == url ('items/'.$item->id.'/edit'))
            <div class="form-group">
                {{ Form::label('pic4','商品圖片4') }}
            </div>
            <div>  
                @if (isset($item->pic4))
                    <!-- <iframe src="{{$item->pic4}}" style="border:none;" height={{"$high"}} width={{"$width"}}></iframe> -->
                    <img src="{{$item->pic4}}" height={{"$high"}} width={{"$width"}}> </img>
                @else
                    <!--<td>{{ Html::image(url('uploads/' . $item->pic4)) }}</td>-->
                    <td></td>
                @endif
            </div>
        @else
        @endif
    </div>
        <!-- 新增商品圖片 file
        @if (isset($errors) and $errors->has('pic'))
            <div class="form-group has-error">
                {{ Form::label('pic','商品圖片(建議尺寸:300*150 pixels)') }}&nbsp;&nbsp;{{ Form::label('pic',$errors->first('pic'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::file('pic',null,['class'=>'form-control']) }}
            </div>
        @else
            <div class="form-group">
                {{ Form::label('pic','商品圖片(建議尺寸:300*150 pixels)') }}
                {{ Form::file('pic',null ,['class'=>'form-control']) }}
            </div>
        @endif-->
        <!--使用者user_id
        @if (isset($errors) and $errors->has('user_id'))
            <div class="form-group has-error">
                {{ Form::label('user_id','使用者id') }}&nbsp;&nbsp;{{ Form::label('user_id',$errors->first('user_id'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::select('user_id',$userIds,$items->user_id,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入使用者id</p>
            </div>
        @else
            <div class="form-group">
                {{ Form::label('user_id','使用者id') }}
                {{ Form::select('user_id',$userIds,null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入使用者id</p>
            </div>
        @endif-->
    </div>
      <div class="col-lg-6">
      
        <!--商品描述-->
        @if (isset($errors) and $errors->has('desc'))
            <div class="form-group has-error">
                {{ Form::label('desc','商品描述') }}&nbsp;&nbsp;{{ Form::label('desc',$errors->first('desc'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::textarea('desc',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入商品描述</p>
            </div>
        @else
            <div class="form-group">
                {{ Form::label('desc','商品描述') }}
                {{ Form::textarea('desc',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入商品描述</p>
            </div>
        @endif
        <!--商品狀態status-->
           <div class="form-group" onChange="showOption()";>
                {{ Form::label('status','商品狀態') }}
                {{ Form::select('status', ['0'=>'銷售中','1'=>'已售出'],null,['class'=>'form-control','rows'=>'3']) }}
           </div>
        <!-- 排序 sort-->
        @if (isset($errors) and $errors->has('sort'))
            <div class="form-group has-error">
                {{ Form::label('sort','排序') }}&nbsp;&nbsp;{{ Form::label('sort',$errors->first('sort'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
            </div>
        @else
            <div class="form-group">
                {{ Form::label('sort','排序') }}
                {{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
            </div>
        @endif
        <!--是否啟用-->
        <div class="form-group">
            {{ Form::label('enabled','是否上架') }}
                {{ Form::label('enabled','是',['class'=>'radio-inline']) }}
                {{ Form::radio('enabled', '1',true) }}
                {{ Form::label('enabled','否',['class'=>'radio-inline']) }}
                {{ Form::radio('enabled', '0') }}
        </div>
        <!--clicksDisable-->
        <div class="form-group">
            {{ Form::label('clicks','點擊數') }}
            {{ Form::text('clicks',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $item->clicks]) }}
        </div>
        <!-- 對應標籤target-->
        @if (isset($errors) and $errors->has('target'))
            <div class="form-group has-error" id = 'target_tag' name='target' >
                {{ Form::label('target','選擇標籤') }}&nbsp;&nbsp;{{ Form::label('target',$errors->first('target'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::select('target',$targetLists,$banner->target,['class'=>'form-control']) }}
                <p class="help-block">選擇你所想要的標籤</p>
            </div>
        @else
            <div class="form-group" id = 'target_tag' name='target' >
                {{ Form::label('target','選擇標籤') }}
                {{ Form::select('target',$targetLists,null,['class'=>'form-control']) }}
                <p class="help-block">選擇你所想要的標籤</p>
            </div>
        @endif

        <!--商品金額-->
        @if (isset($errors) and $errors->has('price'))
            <div class="form-group has-error">
                {{ Form::label('price','商品金額') }}&nbsp;&nbsp;{{ Form::label('price',$errors->first('price'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::text('price',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入商品金額</p>
            </div>
        @else
            <div class="form-group">
                {{ Form::label('price','商品金額') }}
                {{ Form::text('price',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入商品金額</p>
            </div>
        @endif
        <!-- 按鈕-->
        <div class="form-group">
            {{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
            {{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
        </div>
    </div>

</div>