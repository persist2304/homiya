@extends('layouts.base')
@section('page_heading','下架商品列表')
@section('section')
    @include('vendor.flash.message')

    <br/>
    <br/>

    <table id="tb_items" class="table table-bordered display">
    <thead>
        <tr>
            <th>商品名稱</th>
            <th>商品圖片</th>
            <th>商品價格</th>
            <!--如果網址是items就顯示-->
            @if ($urlNow == url ('items_down/'))
                <th>商品狀態</th>
            <!--若網址是交易成功則顯示-->
            @elseif ($urlNow == url ('countSuccessful/'))
            @endif
            <th>是否開啟</th>
            <th>點擊數</th>
        </tr>
    </thead>
    <tbody>
    @if ($urlNow == url ('items_down/'))
        @foreach ($items_down as $item)
            <tr>
                <td><a href="{{ url('items/' . $item->id . '/edit') }} ">{{ $item->title }}</a></td>
                <!--判斷是否有圖片-->
                @if (isset($item->pic1))
                    <td><img id="next" src="{{$item->pic1}}"/></td>
                @else
                    <td></td>
                @endif

                <td>{{ $item->price }}</td>
                <!--判斷銷售狀況-->
                @if ($item->status == 0)
                    <td>銷售中</td>
                @else
                    <td>已售出</td>
                @endif

                @if ($item->enabled == 1)
                    <td>是</td>
                @else
                    <td>否</td>
                @endif
                <td>{{ $item->clicks }}</td>
            </tr>
        @endforeach
        <!--若網址是交易成功則顯示-->
    @elseif ($urlNow == url ('countSuccessful/'))
        @foreach ($itemSuccessful as $itemSuccessfuls)
            <tr>
                <td>{{ $itemSuccessfuls->title }}</td>
                <!--判斷是否有圖片-->
                @if (isset($itemSuccessfuls->pic)&&$itemSuccessfuls->enabled == 1)
                    <td><img id="next" src="{{$itemSuccessfuls->pic}}"/></td>
                @else
                    <td>{{ Html::image(url($itemSuccessfuls->pic1)) }}</td>    
                @endif

                <td>{{ $itemSuccessfuls->price }}</td>
                @if ($itemSuccessfuls->enabled == 1)
                    <td>是</td>
                @else
                    <td>否</td>
                @endif
                <td>{{ $itemSuccessfuls->clicks }}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
@stop

@section('footer')
    <script type="text/javascript">
    $(document).ready(function()
    {
        var table = $('#tb_items').DataTable(
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate":
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria":
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
                    rowReorder:
                    {
                        selector: 'td:nth-child(2)'
                    },
                    responsive: true
        });
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop