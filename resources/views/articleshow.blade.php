<!DOCTYPE html>
<html>
<head>
	<!-- META -->
	<title>homiya</title>
	<meta charset="UTF-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="description" content="" />
	<style>
	img {
     
     max-width: 100%;
     height: auto;
     
 	}
	</style>
	
	<!-- CSS -->
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('/css/kickstart.css')}}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{asset('/css/style.css')}}" media="all" /> 
	 -->
	<!-- Javascript -->
	<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="{{asset('/js/kickstart.js')}}"></script> -->
</head>
	<body>
		
				@yield('content')
			
	</body>
</html>
