@extends('layouts.plane')

@section('body')
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url ('') }}">Homiya Sever</a>
            </div>

            <div class="navbar-header" id="navbar" >
                <ul class="nav navbar-nav navbar-right">
                    <li>
                    <a href="{{ url('logout') }}">
                      <i class="fa fa-user fa-fw"></i> 登出管理者({{ Auth::user()->username }}) 
                    </a>
                </ul>
            </div>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li {{ (Request::is('*users') ? 'class="active"' : '') }}>
                            <a href="{{ url ('users') }}"><i class="fa fa-users fa-fw"></i> 使用者User</a>
                        </li>
                        <li {{ (Request::is('*items') ? 'class="active"' : '') }}>
                            <a href="{{ url ('items') }}"><i class="fa fa-gift fa-fw"></i> 商品Item</a>
                        </li>
                        <li {{ (Request::is('*items_down') ? 'class="active"' : '') }}>
                            <a href="{{ url ('items_down') }}"><i class="fa fa-gift fa-fw"></i> 商品Item(下架)</a>
                        </li>
                        <li {{ (Request::is('*banners') ? 'class="active"' : '') }}>
                            <a href="{{ url ('banners') }}"><i class="fa fa-list-alt fa-fw"></i> 廣告Banner</a>
                        </li>
                        <li {{ (Request::is('*tags') ? 'class="active"' : '') }}>
                            <a href="{{ url ('tags') }}"><i class="fa fa-list-alt fa-fw"></i> 標籤tags</a>
                        </li>
                        <li {{ (Request::is('*bigDatas') ? 'class="active"' : '') }}>
                            <a href="{{ url ('bigDatas') }}"><i class="fa fa-trophy fa-fw"></i> 數據分析</a>
                        </li>
                         <li {{ (Request::is('*wsTest') ? 'class="active"' : '') }}>
                            <a href="{{ url ('wsTest') }}"><i class="fa fa-trophy fa-fw"></i> 網頁測試</a>
                        </li>
                         <li {{ (Request::is('*articles') ? 'class="active"' : '') }}>
                            <a href="{{ url ('articles') }}"><i class="fa fa-trophy fa-fw"></i> 新增文章</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
           </div>
			<div class="row">  
				@yield('section')
            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
    <script type="text/javascript" language="javascript" src="{{ asset('/js/all.js')}}"></script>
    <!-- 尚不知道原因，只知道這隻js無法被打包到all.js-->
     <script type="text/javascript" language="javascript" src="{{ asset('/js/jquery.dataTables.min.js')}}"></script>
     <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
    @yield('footer')
    
@stop

