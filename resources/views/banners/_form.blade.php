@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6">
        <!-- 廣告名稱title-->
        @if (isset($errors) and $errors->has('title'))
            <div class="form-group has-error">
                {{ Form::label('title','廣告名稱') }}&nbsp;&nbsp;{{ Form::label('title',$errors->first('title'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入廣告名稱</p>
            </div>
        @else
            <div class="form-group">
                {{ Form::label('title','廣告名稱') }}
                {{ Form::text('title',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入廣告名稱</p>
            </div>
        @endif

        <!--加入廣告圖片，在edit呈現-->
        @if (isset($banner) && $urlNow == url ('banners/'.$banner->id.'/edit'))
            <div class="form-group">
                {{ Form::label('pic','原本廣告圖片') }}
            </div>
            <div>
                 @if (isset($banner->pic))
                    <td><img src="{{$banner->pic}}"/></td>
                @else
                    <!--<td>{{ Html::image(url('demo/' . $banner->pic)) }}</td>-->
                    <td></td>
                @endif
            </div>
        @else
        @endif

        <!-- 廣告圖片pic-->
        @if (isset($errors) and $errors->has('pic'))
            <div class="form-group has-error">
                {{ Form::label('pic','廣告圖片(建議尺寸:640*360 pixels)') }}&nbsp;&nbsp;{{ Form::label('pic',$errors->first('pic'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::file('pic',null,['class'=>'form-control']) }}
            </div>
        @else
            <div class="form-group">
                {{ Form::label('pic','廣告圖片(建議尺寸:640*360 pixels)') }}
                {{ Form::file('pic',null ,['class'=>'form-control']) }}
            </div>
        @endif

        <!--開啟模式mode-->
           <div class="form-group" onChange="showOption()";>
                {{ Form::label('mode','開啟模式') }}
                {{ Form::select('mode', ['0'=>'開啟網頁','1'=>'某標籤的列表','2'=>'某商品詳細頁面','3'=>'某店家詳細頁面'],null,['class'=>'form-control','rows'=>'3']) }}
           </div>

        <!-- 網址Url-->

        @if (isset($errors) and $errors->has('url'))
            <div class="form-group has-error" id = 'url' >
                {{ Form::label('url','載入網址') }}&nbsp;&nbsp;{{ Form::label('url',$errors->first('url'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::text('url',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入需要載入的網址</p>
            </div>
        @else
            <div class="form-group" id = 'url'>
                {{ Form::label('url','載入網址') }}
                {{ Form::text('url',null,['class'=>'form-control','rows'=>'3']) }}
                <p class="help-block">請輸入需要載入的網址</p>
            </div>
        @endif

        <!-- 對應標籤target-->
        @if (isset($errors) and $errors->has('target') and isset($banner))
            <div class="form-group has-error" id = 'target_tag' name='target' >
                {{ Form::label('target','選擇標籤') }}&nbsp;&nbsp;{{ Form::label('target',$errors->first('target'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::select('target_tag',$targetLists,$banner->target,['class'=>'form-control']) }}
                <p class="help-block">選擇你所想要的標籤</p>
            </div>
        @else
            @if (isset($target))
                <div class="form-group" id = 'target_tag' name='target' >
                    {{ Form::label('target','選擇標籤') }}
                    {{ Form::select('target_tag',$targetLists,$target,['class'=>'form-control']) }}
                    <p class="help-block">選擇你所想要的標籤</p>
                </div>
            @else
                <div class="form-group" id = 'target_tag' name='target' >
                    {{ Form::label('target','選擇標籤') }}
                    {{ Form::select('target_tag',$targetLists,null,['class'=>'form-control']) }}
                    <p class="help-block">選擇你所想要的標籤</p>
                </div>
            @endif
            
        @endif

        <!-- 對應商品target-->
        @if (isset($errors) and $errors->has('target') and isset($banner))
            <div class="form-group has-error" id = 'target_item' name='target' >
                {{ Form::label('target','選擇商品') }}&nbsp;&nbsp;{{ Form::label('target',$errors->first('target'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::select('target_item',$itemsLists,$banner->target,['class'=>'form-control']) }}
                <p class="help-block">選擇你所想要的商品</p>
            </div>
        @else
            @if (isset($target))
                <div class="form-group" id = 'target_item' name='target' >
                    {{ Form::label('target','選擇商品') }}
                    {{ Form::select('target_item',$itemsLists,$target,['class'=>'form-control']) }}
                    <p class="help-block">選擇你所想要的商品</p>
                </div>
            @else
                <div class="form-group" id = 'target_item' name='target' >
                    {{ Form::label('target','選擇商品') }}
                    {{ Form::select('target_item',$itemsLists,null,['class'=>'form-control']) }}
                    <p class="help-block">選擇你所想要的商品</p>
                </div>
            @endif
        @endif

        <!-- 對應使用者target-->
        @if (isset($errors) and $errors->has('target') and isset($banner))
            <div class="form-group has-error" id = 'target_user' name='target' >
                {{ Form::label('target','選擇使用者') }}&nbsp;&nbsp;{{ Form::label('target',$errors->first('target'),['class'=>'control-label','for'=>'inputError']) }}
                {{ Form::select('target_user',$usersLists,$banner->target,['class'=>'form-control']) }}
                <p class="help-block">選擇你所想要的使用者</p>
            </div>
        @else
            @if (isset($target))
                <div class="form-group" id = 'target_user' name='target' >
                    {{ Form::label('target','選擇使用者') }}
                    {{ Form::select('target_user',$usersLists,$target,['class'=>'form-control']) }}
                    <p class="help-block">選擇你所想要的使用者</p>
                </div>
            @else
                <div class="form-group" id = 'target_user' name='target' >
                    {{ Form::label('target','選擇使用者') }}
                    {{ Form::select('target_user',$usersLists,null,['class'=>'form-control']) }}
                    <p class="help-block">選擇你所想要的使用者</p>
                </div>
            @endif
        @endif

        <script type="text/javascript">
        //Page載入完成...
        $(document).ready(function(){
            hideAll();
            showOption();
            // document.getElementById("url").style.display="inline";
        });

        //隱藏所有target選項
        function hideAll(){
            var target_url = document.getElementById("url");
            var target_tag = document.getElementById("target_tag");
            var target_item = document.getElementById("target_item");
            var target_user = document.getElementById("target_user");
            target_url.style.display="none";
            target_tag.style.display="none";
            target_item.style.display="none";
            target_user.style.display="none";
        }

        //顯示與Mode選項關聯的欄位
        function showOption()
        {
            var value = document.getElementById("mode").value;
            hideAll();
            var target;
            if( value == 0 ){
                //選取網頁
                target = document.getElementById("url");
            }else if( value == 1 ){
                //選取標籤
                target = document.getElementById("target_tag");
            }else if( value == 2 ){
                //選取商品
                target = document.getElementById("target_item");
            }else if( value == 3 ){
                //選取店家
                target = document.getElementById("target_user");
            }else{
                alert("Mode選項異常");
            }
            target.style.display="inline";
        }
        </script>
    </div>
      <div class="col-lg-6">
        <!-- 排序 sort-->
        @if (isset($errors) and $errors->has('sort'))
            <div class="form-group has-error">
                {{ Form::label('sort','排序(越小越前面)') }}&nbsp;&nbsp;{{ Form::label('sort',$errors->first('sort'),['class'=>'control-label','for'=>'inputError']) }}
                @if (isset($banner))
                    {{ Form::text('sort',$banner->sort,['class'=>'form-control']) }}
                @else
                    {{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
                @endif
                {{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
            </div>
        @else
            <div class="form-group">
                {{ Form::label('sort','排序(越小越前面)') }}
                @if (isset($banner))
                    {{ Form::text('sort',$banner->sort,['class'=>'form-control']) }}
                @else
                    {{ Form::text('sort',0,['class'=>'form-control','placeholder'=>'0']) }}
                @endif
            </div>
        @endif

        <div class="form-group">
            {{ Form::label('enabled','是否啟用') }}
                {{ Form::label('enabled','是',['class'=>'radio-inline']) }}
                {{ Form::radio('enabled', '1',true) }}
                {{ Form::label('enabled','否',['class'=>'radio-inline']) }}
                {{ Form::radio('enabled', '0') }}
        </div>
 
    <!--點擊數不能按-->
    @if ($urlNow == url('banners/create'))
        
    @else
         <!--點擊數不能按-->
         <div class="form-group">
            {{ Form::label('clicks','點擊數') }}
            {{ Form::text('clicks',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $banner->clicks]) }}
        </div>
    @endif

        <!-- 按鈕-->
        <div class="form-group">
            {{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
            {{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
        </div>
    </div>

</div>