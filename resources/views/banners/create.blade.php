@extends('layouts.base')
@section('page_heading','建立新廣告')
@section('section')
<div class="col-sm-12">
{{ Form::open(['action'=>'BannerController@store','role'=>'form','files'=>true]) }}
	@include('banners._form',['submitBtnText'=>'建立'])
{{ Form::close() }}
</div>          
           
            
@stop
