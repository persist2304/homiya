@extends('layouts.base')
@section('page_heading','廣告列表')
@section('section')
    @include('vendor.flash.message')

    {{ Form::open(['url'=>'banners/create' , 'method'=>'GET']) }}
    {{ Form::submit('新增廣告',['class'=>'btn btn-info ']) }}
    {{ Form::close() }}
    
    <br/>
    <br/>

    <table id="tb_banners" class="table table-bordered display">
    <thead>
        <tr>
            <th>廣告排序</th>
            <th>廣告名稱</th>
            <th>廣告圖片</th>
            <th>開啟模式</th>
            <th>點擊數</th>
            <th>是否開啟</th>
            <th>創造時間</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($banners as $banner)
            <tr>
                <td> {{ $banner->sort }}</td>
                <td><a href="{{ url('banners/' . $banner->id . '/edit') }} ">{{ $banner->title }}</a></td>
                <!--判斷是否網址為banner/edit，呈現圖片-->
                @if (isset($banner->pic))
                    <td><img id="next" src="{{$banner->pic}}"/></td>
                @else
                    <!--<td>{{ Html::image(url('demo/' . $banner->pic)) }}</td>-->
                    <td></td>
                @endif
                @if ($banner->mode == 0)
                    <td>開啟網頁</td>
                @elseif ($banner->mode == 1)
                    <td>某分頁列表</td>
                @elseif ($banner->mode == 2)
                    <td>某商品詳細頁面</td>
                @elseif ($banner->mode == 3)
                    <td>某店家詳細頁面</td>
                @else
                    <td></td>
                @endif
                <td>{{ $banner->clicks }}</td>
                <!--判斷是否開啟-->
                @if ($banner->enabled == 1)
                    <td>是</td>
                @else
                    <td>否</td>
                @endif
                <td>{{ $banner->created_at->format('Y/m/d h:i:s') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('footer')
    <script type="text/javascript">
    $(document).ready(function() 
    {
        var table = $('#tb_banners').DataTable( 
        {

            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },
            "order":[[0,"asc"]],
            //"aaSorting": [[4,'asc'], [3,'desc']],
            // rowReorder: 
            // {
            //     selector: 'td:nth-child(3)'
            // },
            responsive: true
        });
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop