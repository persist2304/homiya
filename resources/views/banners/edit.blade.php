@extends('layouts.base')
@section('page_heading','修改廣告：'.$banner->title)
@section('section')
<div class="col-sm-12">
{{ Form::model($banner,['method'=>'patch','url'=>'banners/'.$banner->id ,'role'=>'form', 'files'=> true]) }}
  @include('banners._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
