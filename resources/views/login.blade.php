@extends ('layouts.plane')
@section ('body')

<div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            <br /><br /><br />
               @section ('login_panel_title','請登入')
               @section ('login_panel_body')
                        @include('vendor.flash.message')
                        <form role="form" method="POST" url="login">
                            <fieldset>
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                <div class="form-group">
                                    <input class="form-control" placeholder="username" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">記住我
                                    </label>
                                </div>
                                <a href="{{url('/password/email')}}">忘記密碼??</a>
                                <!-- Change this to a button or input when using this as a form -->
                               <div class="form-group">
                                    {{ Form::submit('登入',['class'=>'btn btn-primary form-control']) }}
                               </div>
                            </fieldset>
                        </form>
                    
                @endsection
                @include('widgets.panel', array('as'=>'login', 'header'=>true))
            </div>
        </div>
    </div>

        <script>
            $('#flash-overlay-modal').modal();
        </script>
        <script>
            $('div.alert').not('.alert-important').delay(3000).slideUp(300);
        </script>
@stop