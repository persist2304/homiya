@extends('layouts.base')
@section('page_heading','大數據分析')
@section('section')
    @include('vendor.flash.message')

    <br/>
    <br/>
	<div class="row">  
				           
            <!-- /.row -->
            <div class="col-sm-12">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $user_qty }}</div>
                                    <div>目前會員數</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/users')}}">
                            <div class="panel-footer">
                                <span class="pull-left">暸解更多</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-gift fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $item_qty }}</div>
                                    <div>目前商品數</div>
                                </div>
                            </div>
                        </div>
                        
                        <a href="{{url('/items')}}">
                            <div class="panel-footer">
                                <span class="pull-left">瞭解更多</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $deal_qty }}</div>
                                    <div>目前成交數</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/countSuccessful')}}">
                            <div class="panel-footer">
                                <span class="pull-left">暸解更多</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-hand-o-down fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $clicks_qty }}</div>
                                    <div>廣告點擊數</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/banners')}}">
                            <div class="panel-footer">
                                <span class="pull-left">暸解更多</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <!--  圖表區 -->

            <!-- <div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">廣告點擊流量圖</h3>
	
					</div>
		
					<div class="panel-body">
						<canvas height="696" width="1108" id="cline" style="width: 1108px; height: 696px;"></canvas>				
					</div>
				</div>

	 
			</div> -->
			
   
@stop

@section('footer')
    
@stop