<ul>
	<li><a href="service/device/token">取得Token</a></li>

	<li><a href="service/device/register?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&token=086a14&deviceType=ios&check=b9585b05">裝置註冊</a></li>
	<li><a href="service/user/normalLogin?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&email=Haley.Marcellus@Glover.com&password=$2y$10$c.Bhr6XN3GjySOSKA9n63ubGJmgBFQuhhMllzHfENU7YrPFET4aqW&check=b9585b05">一般帳號登入</a></li>
	<li><a href="service/user/autoLogin?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&user_id=68&fb_id=56586&check=b9585b05">自動登入</a></li>
	<li><a href="service/tag/getList?mode=1&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">標籤或分類列表</a></li>
	<li><a href="service/country/getList?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">國家列表</a></li>
	<li><a href="service/zone/getList?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05&country_id=1">區域列表</a></li>
	<li><a href="service/city/getList?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05&zone_id=1">城市列表</a></li>
	<li><a href="service/user/getData?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05&user_id=1&current_user_id=1">某店家資料</a></li>
	<li><a href="service/item/getList?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05&ownUser_id=1&sortMode=0&city_id=1&current_user_id=1&trackingMode=true">商品列表</a></li>
	<li><a href="service/item/getData?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05&item_id=1&current_user_id=1">商品詳細資料</a></li>
	<li><a href="service/item/update?item_id=1&title=test&price=1&desc=hello&qty=2&tags=1,5&city_id=2&status=0&isDelete=true&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">商品變更</a></li>
	<li><a href="service/comment/upload?item_id=2&user_id=1&content=最新資料內容&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">新增評論</a></li>
	<li><a href="service/comment/getList?item_id=1&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">評論列表</a></li>
	<li><a href="service/banner/getList?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">廣告列表</a></li>
	<li><a href="service/banner/click?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05&user_id=1&banner_id=1">點擊廣告</a></li>
	<li><a href="service/fans/getList?deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05&user_id=2&queryUser_id=7&mode=0">粉絲列表</a></li>
	<li><a href="service/fans/upload?user_id=1&followUser_id=2&isFollow=true&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">粉絲追蹤變更</a></li>
	<li><a href="service/like/upload?user_id=1&item_id=2&isLike=true&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">商品喜愛變更</a></li>
	<li><a href="service/like/userList?user_id=1&item_id=96&current_user_id=50&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">商品喜愛用戶清單</a></li>
	<li><a href="service/rating/getList?user_id=1&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">評價列表</a></li>
	<li><a href="service/rating/upload?item_id=2&fromUser_id=1&fromUser_role=1&toUser_id=3&content=test&type=1&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">上傳評價</a></li>
	<li><a href="service/msg/getList?current_user_id=1&user_id=1&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">訊息列表</a></li>
	<li><a href="service/msg/upload?item_id=2&fromUser_id=1&toUser_id=3&room_id=1&content=test&mode=0&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">上傳訊息</a></li>
	<li><a href="service/msg/getNewLetters?user_id=1&refresh_at=5&deviceCode=69e364b0-f7f0-11e5-a837-0800200c9a66&check=b9585b05">取得新訊息數</a></li>


<!-- 	<li><a href="service/help/findname">找name</a></li> -->
</ul>