@extends('layouts.base')
@section('page_heading','修改商品：'.$item->title)
@section('section')
<div class="col-sm-12">
{{ Form::model($item,['method'=>'patch','url'=>'items_down/'.$item->id ,'role'=>'form', 'files'=> true]) }}
  @include('items_down._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
           
            
@stop
