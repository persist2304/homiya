@extends('layouts.base')
@section('page_heading','修改使用者：'.$user->username)
@section('section')
<div class="col-sm-12">
{{ Form::model($user,['method'=>'patch','url'=>'users/'.$user->id ,'role'=>'form']) }}
	@include('users._form',['submitBtnText'=>'修改'])
{{ Form::close() }}
</div>          
                       
@stop


