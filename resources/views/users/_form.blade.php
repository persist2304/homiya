@include('vendor.flash.message')
<div class="row">
    <div class="col-lg-6">
		<!-- 行動電話mobile-->
    	@if (isset($errors) and $errors->has('mobile'))
    		<div class="form-group has-error">
	    		{{ Form::label('mobile','行動電話') }}&nbsp;&nbsp;{{ Form::label('mobile',$errors->first('mobile'),['class'=>'control-label','for'=>'inputError']) }}
				{{ Form::text('mobile',null,['class'=>'form-control']) }}
			</div>
    	@else
    		<div class="form-group">
    			{{ Form::label('mobile','行動電話') }}
				{{ Form::text('mobile',null,['class'=>'form-control']) }}
			</div>
    	@endif
    	
		<div class="form-group">
				{{ Form::label('enabled','是否啟用') }}
				{{ Form::label('enabled','是',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '1',true) }}
				{{ Form::label('enabled','否',['class'=>'radio-inline']) }}
				{{ Form::radio('enabled', '0') }}
		</div>
		<!--UsernameDisable-->
    	<div class="form-group">
    		{{ Form::label('username','Username') }}
			{{ Form::text('username',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->username]) }}
		</div>
        <!--Fb_idDisable-->         
		<div class="form-group">
            {{ Form::label('fb_id','fb_id') }}
            {{ Form::text('fb_id',null ,['class' => 'form-control', 'id' => 'myid', 'disabled' ,'placeholder' => $user->fb_id]) }}
		</div>          
		<!--姓氏firstName disable-->
    	<div class="form-group">
    		{{ Form::label('firstName','姓氏') }}
			{{ Form::text('firstName',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->firstName]) }}
		</div>       
		<!--姓氏lastName disable-->
    	<div class="form-group">
    		{{ Form::label('lastName','名字') }}
			{{ Form::text('lastName',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->lastName]) }}
		</div> 
		<!--最後一次登入 lastLogin disable-->
    	<div class="form-group">
    		{{ Form::label('lastLogin','最後一次登入') }}
			{{ Form::text('lastLogin',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->lastLogin]) }}
		</div>      
		<!--建立時間 created_at disable-->
    	<div class="form-group">
    		{{ Form::label('created_at','建立時間') }}
			{{ Form::text('created_at',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->created_at]) }}
		</div>   
		<!--更新時間 updated_at disable-->
    	<div class="form-group">
    		{{ Form::label('updated_at','更新時間') }}
			{{ Form::text('updated_at',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->updated_at]) }}
		</div>   
    	<!--個人網站disable-->
    	<div class="form-group">
    		{{ Form::label('website','個人網站') }}
			{{ Form::text('website',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->website]) }}
		</div>
		<!--生日disable-->
    	<div class="form-group">
    		{{ Form::label('birthday','生日') }}
			{{ Form::text('birthday',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->birthday]) }}
		</div>

		
	</div>

 	<div class="col-lg-6">
 	<!--姓名name disable-->
   	<div class="form-group">
   		{{ Form::label('name','姓名') }}
		{{ Form::text('name',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->name]) }}
	</div>
	<!--性別gender disable-->
   	<div class="form-group">
   		{{ Form::label('gender','性別') }}
		{{ Form::text('gender',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->gender]) }}
	</div>
 	<!--圖片 pic -->
        @if (isset($user) && $urlNow == url ('users/'.$user->id.'/edit'))
            <div class="form-group">
                {{ Form::label('pic','使用者圖片') }}
            </div>
            <div>
                    <td><img src="{{$user->pic}}"/></td>
            </div>
        @else
        @endif         
	<!--信箱email disable-->
   	<div class="form-group">
   		{{ Form::label('email','信箱') }}
		{{ Form::text('email',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->email]) }}
	</div> 
	<!--地址 address disable-->
    <div class="form-group">
    	{{ Form::label('address','地址') }}
		{{ Form::text('address',null,['class'=>'form-control','id'=>'myid','disabled','placeholder'=> $user->address]) }}
	</div>
	<!--disable自我描述-->
	<div class="form-group">
      	{{ Form::label('desc','自我描述') }}
        {{ Form::textarea('desc',null ,['class' => 'form-control', 'id' => 'myid', 'disabled' ,'placeholder' => $user->desc]) }}
	</div>  
	<div class="col-lg-12">
		<!--評價，優，普，差-->
		<div>
			{{ Form::label('eval_qty','使用者評價：') }}
			{{ Form::label('eval_qty','優：') }} {{$queryEvaluates['userGood']}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ Form::label('eval_qty','普：') }} {{$queryEvaluates['userCommon']}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ Form::label('eval_qty','差：') }} {{$queryEvaluates['userBad']}}
		</div>
	</div>
		<!--按鈕 submit-->
		<div class="form-group">
			{{ Form::submit($submitBtnText,['class'=>'btn btn-primary form-control']) }}
			{{ Form::reset('清除',['class'=>'btn btn-default form-control']) }}
		</div>  
 	</div>		
</div>
</div>