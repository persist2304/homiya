@extends('layouts.base')
@section('page_heading','使用者列表')
@section('section')
    @include('vendor.flash.message')
    
    <br/>
    <br/>

    <table id="tb_users" class="table table-bordered display">
    <thead>
        <tr>
            <th>username</th>
            <th>姓名</th>
            <th>Email</th>
            <th>行動電話</th>
            <th>最後一次登入</th>
            <th>是否開啟</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td><a href="{{ url('users/' . $user->id . '/edit') }} ">{{ $user->username }}</a></td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->mobile }}</td>
                <td>{{ $user->lastLogin }}</td>
                @if ($user->enabled == 1)
                    <td>是</td>
                @else
                    <td>否</td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('footer')
    <script type="text/javascript">
    $(document).ready(function() 
    {
        var table = $('#tb_users').DataTable( 
        {
            "language":
            {
                "decimal":        "",
                "emptyTable":     "沒有任何搜尋紀錄",
                "info":           "顯示 _START_ / _END_ 全部有 _TOTAL_ 筆資料",
                "infoEmpty":      "顯示 0 / 0 全部有 0 筆資料",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "顯示 _MENU_ 筆資料",
                "loadingRecords": "搜尋中...",
                "processing":     "處理中...",
                "search":         "搜尋:",
                "zeroRecords":    "沒有任何資料",
                "paginate": 
                {
                    "first":      "第一頁",
                    "last":       "最後一頁",
                    "next":       "下一頁",
                    "previous":   "上一頁"
                },
                   "aria": 
                   {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                   }
            },

            rowReorder: 
                {
                    selector: 'td:nth-child(2)'
                },



            responsive: true ,

            

                    
        });
    });
    </script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop