<?php
//use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Admin;
use App\Questionnaire;
use App\Question;
use App\Gift;
use App\User;
use App\Order;
use Auth;

class WebServiceTest extends TestCase {
   
	private static $method = 'GET';

	/*簡單測試WS API是否正常運作*/

	public function testServiceDeviceToken(){
		$response = $this->call(self::$method, '/service/device/token');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceDeviceRegister(){
		$response = $this->call(self::$method, '/service/device/register');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceUserRegister(){
		$response = $this->call(self::$method, '/service/user/register');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceUserFbLogin(){
		$response = $this->call(self::$method, '/service/user/fbLogin');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceUserNormalLogin(){
		$response = $this->call(self::$method, '/service/user/normalLogin');
		$this->assertEquals(200, $response->getStatusCode());
	}
	// public function testServiceUserForgetPwd(){
	// 	$response = $this->call(self::$method, '/service/user/forgetPwd');
	// 	$this->assertEquals(200, $response->getStatusCode());
	// }
	public function testServiceUserChangePwd(){
		$response = $this->call(self::$method, '/service/user/changePwd');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceUserUpdate(){
		$response = $this->call(self::$method, '/service/user/update');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceUserGetData(){
		$response = $this->call(self::$method, '/service/user/getData');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceBannerGetList(){
		$response = $this->call(self::$method, '/service/banner/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceTagGetList(){
		$response = $this->call(self::$method, '/service/tag/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceCountryGetList(){
		$response = $this->call(self::$method, '/service/country/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceZoneGetList(){
		$response = $this->call(self::$method, '/service/zone/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceCityGift(){
		$response = $this->call(self::$method, '/service/city/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceItemGetList(){
		$response = $this->call(self::$method, '/service/item/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceItemUpload(){
		$response = $this->call(self::$method, '/service/item/upload');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceItemUpdate(){
		$response = $this->call(self::$method, '/service/item/update');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceItemGetData(){
		$response = $this->call(self::$method, '/service/item/getData');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceFansGetList(){
		$response = $this->call(self::$method, '/service/fans/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceFansUpload(){
		$response = $this->call(self::$method, '/service/fans/upload');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceLikeUpload(){
		$response = $this->call(self::$method, '/service/like/upload');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceLikeUserList(){
		$response = $this->call(self::$method, '/service/like/userList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceCommentGetList(){
		$response = $this->call(self::$method, '/service/comment/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceCommentUpload(){
		$response = $this->call(self::$method, '/service/comment/upload');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceMsgGetList(){
		$response = $this->call(self::$method, '/service/msg/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceMsgUpload(){
		$response = $this->call(self::$method, '/service/msg/upload');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceMsgGetNewLetters(){
		$response = $this->call(self::$method, '/service/msg/getNewLetters');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceRatingGetList(){
		$response = $this->call(self::$method, '/service/rating/getList');
		$this->assertEquals(200, $response->getStatusCode());
	}
	public function testServiceRatingUpload(){
		$response = $this->call(self::$method, '/service/rating/upload');
		$this->assertEquals(200, $response->getStatusCode());
	}
}
