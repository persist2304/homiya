<?php 

use App\Admin;
use App\User;
use App\Item;
use App\Banner;
use App\Tag;
use Illuminate\Support\Facades\Auth;

class SiteTest extends TestCase
{
	public function testHomesPage()
	{
		$this->loginAdmin();
		$response = $this->call('GET', '/');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testUsersIndex()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/users');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testUserEdit()
	{
		$this->loginAdmin();
		$user = User::all()->first();
		//dd('/users/' . $user->id .'/edit');
		$response = $this->call('GET', '/users/' . $user->id .'/edit');
		//dd($response);
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testItemsIndex()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/items');
		$this->assertEquals(200,$response->getStatusCode());
	}

	public function testItemEdit()
	{
		$this->loginAdmin();
		$item = Item::all()->first();
		$response = $this->call('GET','/items/'.$item->id.'/edit');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testBannersIndex()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/banners');
		$this->assertEquals(200,$response->getStatusCode());
	}	

	public function testBannerCreate()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/banners/create');
		$this->assertEquals(200,$response->getStatusCode());
	}

	public function testBannerEdit()
	{
 		$this->loginAdmin();
		$banner = Banner::all()->first();
		$response = $this->call('GET','/banners/'.$banner->id.'/edit');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testTagsIndex()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/tags');
		$this->assertEquals(200,$response->getStatusCode());
	}	

	public function testTagCreate()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/tags/create');
		$this->assertEquals(200,$response->getStatusCode());
	}

	public function testTagEdit()
	{
 		$this->loginAdmin();
		$tag = Tag::all()->first();
		$response = $this->call('GET','/tags/'.$tag->id.'/edit');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testBigDatasIndex()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/bigDatas');
		$this->assertEquals(200,$response->getStatusCode());
	}

	public function testRestPassword()
	{
		$response = $this->call('GET', '/password/email');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testLoginPage()
	{
		$response = $this->call('GET', '/login');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testCountSuccessful()
	{
		$this->loginAdmin();
		$response = $this->call('GET','/countSuccessful');
		$this->assertEquals(200,$response->getStatusCode());
	}

	/*以管理者身份登入*/
	private function loginAdmin()
	{
		$admin = Admin::where('username','admin')->first();
		Auth::loginUsingId($admin->id);
	}
}
?>