<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluate extends Model 
{
	protected $table = 'evaluates';
	protected $fillable = ['item_id','role','giver_id','receiver_id','type','content'];

	protected $casts = [
		'id' => 'integer',
		'item_id' => 'integer',
		'role' => 'integer',
		'giver_id' => 'integer',
		'receiver_id' => 'integer',
		'type' => 'integer',
	];

	//每個評價只屬於某個商品
	public function item()
	{
		return $this->hasOne('App\Item');
	}

}