<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model 
{
	protected $table = 'follows';

	public $timestamps = false;

	protected $fillable = ['user_id','fan_id'];

	protected $casts = [
		'id' => 'integer',
		'user_id' => 'integer',
		'fan_id' => 'integer',
	];
}
