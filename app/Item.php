<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model 
{
	protected $table = 'items';

	protected $fillable = ['title','user_id','price','city_id','desc','status','sort','enabled','clicks','pic1','pic2','pic3','pic4','tags','clicks','qty','sort'];

	protected $casts = [
		'id' => 'integer',
		'sort' => 'integer',
    	'price' => 'integer',
    	'user_id' => 'integer',
    	'city_id' => 'integer',
    	'status' => 'integer',
    	'enabled' => 'integer',
    	'clicks' => 'integer',
    	'qty' => 'integer',
	];

	//商品擁有很多個標籤
	public function tags()
	{
		return $this->belongsToMany('App\Tag')->withTimestamps();
	}

	//商品擁有很多個評價
	public function evaluates()
	{
		return $this->hasMany('App\Evaluate');
	}
	//商品只會屬於一個使用者
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}