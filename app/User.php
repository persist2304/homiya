<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username','name','password','gender','fb_id','email','firstName','lastName','city_id','desc','pic','website','mobile','birthday','address','lastLogin','enabled','likes'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = ['remember_token'];

	protected $casts = [
		'id' => 'integer',
    	'city_id' => 'integer',
    	'enabled' => 'integer',
	];
	
	protected $dates = ['created_at', 'updated_at', 'birthday'];

	// 回傳該使用者現在位於哪個城市
	public function city()
	{
		return $this->belongsTo('App\City');
	}

	//回傳該使用者擁有哪些商品
	public function items()
	{
		return $this->hasMany('App\Item');
	}

	//回傳該使用者擁有幾筆訊息
	public function messages()
	{
		return $this->hasMany('App\Message');
	}

	//回傳該使用者擁有幾筆評論
	public function comments()
	{
		return $this->hasMany('App\Comment');
	}
}
