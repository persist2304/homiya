<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model 
{
	protected $table = 'messages';

	protected $fillable = ['item_id','fromUser_id','toUser_id','content','mode','room_id','bid','enabled','isRead'];

	protected $casts = [
		'id' => 'integer',
		'item_id' => 'integer',
		'fromUser_id' => 'integer',
		'toUser_id' => 'integer',
		'mode' => 'integer',
		'room_id' => 'integer',
		'bid' => 'integer',
		'enabled' => 'integer',
		'isRead' => 'integer',
	];

	//訊息只屬於某個商品
	public function item()
	{
		return $this->belongsTo('App\Item');
	}

	//每個訊息只屬於某個使用者
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}