<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model 
{
	protected $table = 'tags';

	protected $fillable = ['mode','title','sort','enabled','pic'];

	protected $casts = [
		'id' => 'integer',
		'mode' => 'integer',
		'sort' => 'integer',
		'enabled' => 'integer',
	];

	//標籤可以用有很多商品
	public function items()
	{
		return $this->belongsToMany('App\Item')->withTimestamps();
	}
}