<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model 
{
	protected $table = 'countries';

	protected $fillable = ['title','sort','enabled'];

	protected $casts = [
		'id' => 'integer',
		'sort' => 'integer',
		'enabled' => 'integer',
	];

	public $timestamps = false;

	//每個國家包括多個區域
	public function zones()
	{
		return $this->hasMany('App\Zone');
	}
}