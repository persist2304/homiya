<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model 
{
	protected $table = 'comments';

	protected $fillable = ['item_id','content','user_id','enabled'];

	protected $casts = [
		'id' => 'integer',
		'item_id' => 'integer',
		'user_id' => 'integer',
		'enabled' => 'integer',
	];

	//每個評價只屬於某個使用者
	public function users()
	{
		return $this->belongsTo('App\User');
	}

	//每個評價只屬於某個使用者
	public function item()
	{
		return $this->belongsTo('App\Item');
	}
}
