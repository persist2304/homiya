<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {

	protected $table = 'rooms';

	protected $fillable = ['item_id','user1','user2'];

	public $timestamps  = false;

	//房間只屬於某個商品
	public function item()
	{
		return $this->belongsTo('App\Item');
	}

}
