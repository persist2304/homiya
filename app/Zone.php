<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model 
{
	protected $table = 'zones';

	public $timestamps = false;

	protected $fillable = ['title','country_id','sort','enabled'];

	protected $casts = [
		'id' =>'integer',
		'country_id' => 'integer',
		'sort' => 'integer',
		'enabled' => 'integer',
	];

	//每個區域有很多個城市
	public function cities()
	{
		return $this->hasMany('App\City');
	}

	//每個區域只有一個國家
	public function country()
	{
		return $this->belongsTo('App\County');
	}
}