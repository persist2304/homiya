<?php namespace App\Http\Model;


use DB;
use App\Item;
use App\Track;
use App\Tag;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Input;
use Image;
use Carbon\Carbon;
use DOMDocument;
use DOMXPath;

class PublicUtil{



/*
------------------------------------------------------------------------------------------------------------
以下是專案使用
------------------------------------------------------------------------------------------------------------
*/

	 /*
	  *  生成隨機的檔名
	  *  $qty 可傳入檔名的長度
	  */
	public static function randomFileName($qty)
	{
		$ran_chars = '1234567890abcdefghijklmnopqrstuvwxyz';
		$ran_string = '';
		for($i = 0; $i < $qty; $i++){
			$ran_string .= $ran_chars[rand(0, 35)];
		}
		return $ran_string;
	}

	/*處理傳入的圖片路徑，去除public/uploads/之後回傳圖片檔名
	 *$imagePath   位於public/uploads/裏頭的圖片路徑
	 */
	public static function getImageName($imagePath)
	{
	    $image_name = str_replace('public/uploads/' , '' , $imagePath);
	    return $image_name;
	}

	/*
	 * 根據生日計算年齡
	 * @param $birthday 格式為MM/DD/YYYY
	 * @return int 年齡
	 */
	public static function countAge($birthDate)
	{
  		//explode the date to get month, day and year
  		$birthDate = explode("/", $birthDate);
  		//get age from date or birthdate
  		$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
  		return $age;
	}

	/*
		壓縮圖片檔案，先在本地端下載在uploads後，並上傳到s3雲端
		$request 使用的request方法
	*/
	public static function picUpload(Request $request , $picName = 'pic' , $width = false , $height = false)
	{
		//dd($request);
		$inputs = $request ->all();
		$file = [
  					$picName => $request->file($picName),
	  			 ];
	  	$isFail = false;
	  	//dd($file);
	  	foreach ($file as $key => $value) 
	  	{
	  		if (isset($value)) 
	  		{
	  			//dd($key);
	  			if ($value->isValid()) 
	  			{
	    			$destinationPath = 'uploads'; // upload path
		   			$extension = $value->getClientOriginalExtension(); // getting image extension
	     			$fileName = PublicUtil::randomFileName(12) .'.' . $extension; // renameing image
	     			Input::file($picName)->move($destinationPath, $fileName); // uploading file to given path
	     			$data['filename'] = $fileName;//因為表格內沒有這個屬性，所以要自行新增
	     			$img = Image::make('uploads/'.$data['filename'])->orientate()->resize($width, $height)->save();//上傳後規定的大小
	     			Storage::disk('s3')->put($fileName, ($img),'public');
	      			$file[$key] = $fileName;
		    	}
		    	else
		    	{
		    		// sending back with error message.
		    		$isFail = true;
		   			$file[$key] = null;
		   		}
	  		}
	 	}
	 		if (!$isFail) 
	 		{
	  			return $file;
	  		}
	  		else
	  		{
	  			return null;
	  		}
	}
	/*
		主要為選擇你想要刪除的檔案
		$dirName 刪除檔案名稱，或者資料夾
	*/
	public static function delFile($dirName)
	{
		if(file_exists($dirName) && $handle=opendir($dirName))
		{
			while(false!==($item = readdir($handle)))
			{
			if($item!= "." && $item != "..")
			{
				if(file_exists($dirName.'/'.$item) && is_dir($dirName.'/'.$item))
				{
					delFile($dirName.'/'.$item);
				}
				else
				{
					if(unlink($dirName.'/'.$item))
					{
						return true;
					}
				}
			}
		}
			closedir( $handle);
		}
	}

	/*
		計算messages表格的最大id值，用以作為refresh_at使用
	*/
	public static function queryRefresh_at()
	{
		$msgs_refreshAts = DB::select('select max(id) as refresh_at from messages');
		$refresh_at =  $msgs_refreshAts[0]->refresh_at;
		if ($refresh_at == null) {
			$refresh_at = "0";
		}
		return intval($refresh_at);
	}

	/*
		根據條件進行排序，並可根據價格區間.標籤.關鍵字來過濾商品，並回傳符合商品陣列
	*/
	public static function returnPricedItemsWithSorted($inputs, $sortType, $sort) {
		//處理where指令
		//先過濾價格
		$sql_where = '';
		if (isset($inputs['priceMax'])) {
			$sql_where = $sql_where . " a.price <= " . $inputs['priceMax'];
		}
		if (isset($inputs['priceMin'])) {
			if (strlen(trim($sql_where))>0) {
				$sql_where = $sql_where . " and";
			}
			$sql_where = $sql_where . " a.price >= " . $inputs['priceMin'];
		}

		//過濾標籤
		if (isset($inputs['tags'])) {
			$tags = explode(',' , $inputs['tags']);
			for ($i=0; $i < count($tags) ; $i++) { 
				if (strlen(trim($sql_where))>0) {
					$sql_where = $sql_where . " or";
				}
				$sql_where = $sql_where . ' a.tags like "%,' . $tags[$i] . ',%"';
			}
			//dd($sql_where);
		}

		//過濾城市
		if ($inputs['city_id'] != -1) {
			if (strlen(trim($sql_where))>0) {
				$sql_where = $sql_where . " and";
			}
			$sql_where = $sql_where . " a.city_id = " . $inputs['city_id'] ;
		}

		//過濾關鍵字
		if (isset($inputs['keyword'])) {
			if (strlen(trim($sql_where))>0) {
				$sql_where = $sql_where . " and";
			}
			$sql_where = $sql_where . '( a.title like "%' . $inputs['keyword'] . '%" or a.user_id in ( select id from `users` where name like "%' . $inputs['keyword'] . '%")) and a.enabled = 1';
		}

		//過濾是否為追蹤
		if ($inputs['trackingMode'] == 1) {
			if (strlen(trim($sql_where))>0) {
				$sql_where = $sql_where . " and";
			}
			$tracks = Track::where('user_id', $inputs['current_user_id'])->get();
			//$track_ary = '';
			if (count($tracks) > 0) {
				for ($i=0; $i < count($tracks); $i++) { 
					if ($i == 0) {
						$track_ary = '(';
					}

					$track_ary = $track_ary . $tracks[$i]->item_id;


					if ( $i != count($tracks)-1 ) {
						$track_ary = $track_ary . ',';
					}

					if ($i == count($tracks)-1) {
						$track_ary = $track_ary  . ')';
					}
				}
			}else{
				//沒有追蹤任何東西
				return [];
			}
				
			$sql_where = $sql_where . ' a.id in ' . $track_ary;
			//dd($sql_where);
		}

		//接著排序
		if ($sortType == 'price' || $sortType == 'clicks' || $sortType == 'created_at' ) {
			if (strlen(trim($sql_where)) > 0) {
				$items = DB::select("select * from items as a where a.enabled=1 and " . $sql_where . " order by " . $sortType . " " . $sort);
			}else{
				$items = DB::select("select * from items as a where a.enabled=1 order by " . $sortType . " " . $sort);
			}
			
		}elseif($sortType == 'user_rank'){
			//SELECT a.* , b.fan_count as user_rank from items as a left join (select user_id,count(`user_id`) as fan_count from follows group by user_id ) as b on a.user_id = b.user_id order by user_rank desc
			if (strlen(trim($sql_where)) > 0) {
				$items = DB::select('select a.* , b.fan_count as user_rank from items as a left join (select user_id,count(`user_id`) as fan_count from follows group by user_id ) as b on a.user_id = b.user_id where a.enabled=1 and ' . $sql_where . ' order by user_rank ' . $sort);
			}else{
				$items = DB::select('select a.* , b.fan_count as user_rank from items as a left join (select user_id,count(`user_id`) as fan_count from follows group by user_id ) as b on a.user_id = b.user_id where a.enabled=1 order by user_rank ' . $sort);
			}
		}elseif($sortType == 'distance'){
			//暫未完成
		}
		return $items;

	}

	//製作標籤字串，在標籤字串的頭和尾加上,。$tagsStr為待處理的標籤字串
	public static function buildTagString( $tagsStr){
		if (strlen(trim($tagsStr)) > 0) {
			$tags = explode(',', $tagsStr);
			$tag_str = ',';
			for ($i=0; $i < count($tags); $i++) { 
				$tag_str = $tag_str  . $tags[$i] . ',';
			}
			return $tag_str;
		}else{
			return null;
		}
	}

	//將標籤ids字串，轉換成標籤標題字串
	public static function transferTagIdsToTagTitles( $tagsStr){
		if (isset($tagsStr) && strlen($tagsStr) > 0) {
			$str_tags = substr($tagsStr , 1 , count($tagsStr)-2);
			$tag_ary = explode(',' , $str_tags);
			for ($i=0; $i < count($tag_ary) ; $i++) { 
				$tag = Tag::findOrFail($tag_ary[$i]);
				$tag_ary[$i] = $tag->title;
			}
			return implode("," , $tag_ary);
		}else{
			return null;
		}
	}
/*
------------------------------------------------------------------------------------------------------------
以下是 公共使用 使用
------------------------------------------------------------------------------------------------------------
*/
	/*
	 * 建立FB大頭照網址
	 */
	public static function buildFbPicPath($fb_id)
	{
		return 'https://graph.facebook.com/' . $fb_id . '/picture?type=large';
	}

	// /*
	//  * 將時間字串轉換成Carbon型別
	//  */
	// public static function timeToCarbon($time)
	// {
	// 	return Carbon::parse($time);
	// }

	//將Carbon型別轉換為日期格式
	public static function carbonToDate($carbon)
	{
		return $carbon->format('m/d/Y');
	}

	//檢查某字串的開頭是否為該子字串,$haystack為受檢查字串，$needle為要比對字串
	public static function startsWith($haystack, $needle) {
	    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}

	//檢查某字串的結尾是否為該子字串,$haystack為受檢查字串，$needle為要比對字串
	public static function endsWith($haystack, $needle) {
	    // search forward starting from end minus needle length characters
	    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}

	public static function getImgToS3($html)
	{
		if ($html == null) 
		{
			//
		}
		else
		{
			//只取得img部份
			$doc = new DOMDocument();
			libxml_use_internal_errors(true);
			$doc->loadHTML($html); // loads your html
			$xpath = new DOMXPath($doc);
			$nodelist = $xpath->query("//img"); // find your image
			$isImg = $nodelist->length;
			if ($isImg == '0') 
			{
				return $html;
			}
			else
			{
				for ($i=0; $i < $isImg ; $i++) 
				{ 
					$node = $nodelist->item($i); // gets the 1st image
					$value = $node->attributes->getNamedItem('src')->nodeValue;
					//如果上傳的圖片是從不是檔案上傳的
					if (false !== strpos($value, 'http'))
					{
						return $html;
					}
					else
					{
						$getUrlType = getimagesize($value);
						$imgType = substr($getUrlType['mime'], 6);
						$rename = PublicUtil::randomFileName(20);
						$localImgSave = 'uploads/'.$rename.'.'.$imgType;	
						$uploadS3Name = $rename.'.'.$imgType;
						$img    = Constant::getS3Path().$rename.'.'.$imgType;
						$file   = file($value);
						$result = file_put_contents($localImgSave, $file);						
						$html = str_replace($value,$img,$html);
						// dd($value);
						Storage::disk('s3')->put($uploadS3Name,fopen($value,"w"),'public');//上傳直接下載下來
						// Storage::disk('s3')->put($uploadS3Name,$value,'public');//上傳直接下載下來
						//<目前使用資料庫存取，並未將檔案生成為html檔案，所以暫時註解掉>
						// $language = '<meta charset="UTF-8">';
						// $htmlFileName = $rename.'.html';
						// $fp = fopen ('temps/'.$htmlFileName,"w");
						// $result = fwrite($fp,$language.$html);
						// Storage::disk('s3')->put($htmlFileName,$language.$html,'public');//html檔案上傳直接下載下來
						PublicUtil::delFile('uploads');
					}	
				}
				return $html;
			}
		}		
	}

	
}