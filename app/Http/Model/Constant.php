<?php 

namespace App\Http\Model;
use App\Http\Model\PublicUtil;
use Input;
use App;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class Constant
{
	/*
		取得s3雲端硬碟的網址
	*/
	public static function getS3Path(){
		$s3Path = "https://s3.amazonaws.com/homiya/";
		return $s3Path;
	}

	//預設用戶介紹文字
	public static function getDefaultDesc(){
		return '該用戶尚未填寫自我介紹';
	}

	//預設商品介紹文字
	public static function getDefaultItemDesc(){
		return '該商品尚無任何介紹';
	}

	//預設商品排序
	public static function getDefaultSort(){
		return 0;
	}

	//預設商品是否開啟
	public static function getDefaultItemEnabled(){
		return 1;
	}

	//預設用戶排序
	public static function getDefaultUserSort(){
		return 0;
	}
/*
------------------------------------------------------------------------------------------------------------
以下是WebServer
------------------------------------------------------------------------------------------------------------
*/
	/*
		取得serverIp
	*/
	public static function showIp(){
		$ip = App::environment('local') ? 'localhost' : '52.35.133.107';
		return $ip;
	}
	/*
		取得本地資料夾
	*/
	public static function  localFolder(){
		$local = App::environment('local') ? 'homiyaLocal' : 'homiya';
		return $local;
	}
}
?>