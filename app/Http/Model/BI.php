<?php 
namespace App\Http\Model;
use App\Banner;
use App\Evaluate;
use App\Item;
use App\Room;
use DB;

class BI
{
	//根據所提供的使用者id以及評價類型來查詢評價數
	//$userId 使用者流水號
	//type 評價類型，1為優，2為普，3為差
	//return 評價數
	public static function queryEvaluate($userId , $type)
	{
		//舊版
		// $eval_qty = Evaluate::whereIn('item_id', function($query) use( $userId , $type ){
  //   	$query->select('id')
  //   	->from(with(new Item)->getTable())
  // 	  	->where('user_id', $userId);
		// })->where('type','=',$type)->count();
		$evaluates = Evaluate::where('receiver_id',$userId)->where('type',$type)->get();
		if(isset($evaluates)){
			$eval_qty = count($evaluates);
		}else{
			$eval_qty = 0;
		}
		return $eval_qty;
	}
	/*
		1.取得商品頁面已銷售數量
		2.$sellOut 取得商品已銷售商量
		3.回傳 $sellOut
	*/
	public static function sellOut(){
		$sellOut = Item::where('status','=','1')->get();
		return $sellOut;
	}

	/*
	 * 生成驗證碼，規則為X+B13EDF+HOMIYA1+$secret1+/+$secret2進行sha256編碼後取第2~9位
	 */
	public static function buildCheck($secret1 , $secret2){
		$sha256 = hash('sha256', ('X' . 'B13EDF' . 'HOMIYA1' . $secret1 . '/' . $secret2));
		$check = substr( $sha256 , 2 , 8);
		//dd('check:' . $check );
		return $check;
	}

	//計算某用戶對某商品的最新出價金額，如未出過價或者出價後取消出價則回傳null
	//item_id 商品流水號
	//user_id 如提供用戶流水號表示欲查詢該用戶的最高出價金額流水號
	//room_id 如提供房間id ，表示要查詢該房間的最高出價金額
	//回傳最高出價金額 Int
	public static function queryLastBid( $item_id , $user_id = null , $room_id = null){
		if($user_id != null){
			$sql = "select * from messages where item_id = " . $item_id . " and fromUser_id = " . $user_id . " and created_at = (select max(created_at) from messages where item_id = " . $item_id . " and fromUser_id = " . $user_id . " and (mode=1 or mode=2 or mode=4))";
		}else if($room_id != null){
			$sql = "select * from messages where room_id = " . $room_id . " and created_at = (select max(created_at) from messages where room_id = " . $room_id . " and (mode=1 or mode=2 or mode=4))";
		}
		
		//$sql = "select max(bid) as bid from messages where mode = 1 and item_id = " . $item_id . " and fromUser_id = " . $current_user_id;
		//dd(DB::select($sql));
		$result = DB::select($sql);
		//dd($result);
		if (isset($result) && count($result) == 1) {
			return $result[0]->bid;
		}else{
			return null;
		}
	}

	//查詢某商品的出價者數量
	//item_id 商品流水號
	//回傳出價者數量 Int
	public static function queryItemBiders( $item_id){
		$sql = "select count(distinct fromUser_id) as biders from messages where mode = 1 and item_id = " . $item_id;
		$result = DB::select($sql);
		return $result[0];
	}

	//計算聊天室mode，分別為S0~4(賣家)，以及B0~3(買家)
	//room_id 聊天室流水號
	//current_user_id 當前使用者流水號
	//Return 回傳聊天室Mode String
	public static function queryRoomMode( $room_id , $current_user_id){
		$room = Room::find($room_id);
		if (!isset($room)) {
			return $this->makeWsFormat(null, 0, "所查詢聊天室並不存在");
		}else{
			$item = $room->item;
			if ($item->user_id == $current_user_id) {
				//為賣家
				$mode_pre = "S";
			}else{
				//為買家
				$mode_pre = "B";
			}

			if ($item->status == 1) {
				//該商品已經成交
				$mode_suffix = "2";
				if ($mode_pre == "S") {
					//判斷是否寫過評價
					$sql = "SELECT * from evaluates where item_id =" . $item->id . " and role = 1 and giver_id = " . $current_user_id;
					$evaluates = DB::select($sql);
					if (isset($evaluates) && count($evaluates) > 0) {
						$mode_suffix = "3";
					}else{
						$mode_suffix = "2";
					}
					
				}else{
					//select * from messages where item_id = 1 and fromUser_id = 1 and toUser_id = 2 and mode = 3
					$sql = "select * from messages where item_id = " . $item->id . " and fromUser_id = " . $item->user_id  . " and toUser_id = " . $current_user_id . " and mode = 3";
					$results = DB::select($sql);
					if (count($results) == 0) {
						//使用者非得標人
						$mode_suffix = "3";
					}else{
						//判斷是否寫過評價
						$sql = "SELECT * from evaluates where item_id =" . $item->id . " and role = 2 and giver_id = " . $current_user_id;
						$evaluates = DB::select($sql);
						if (isset($evaluates) && count($evaluates) > 0) {
							$mode_suffix = "4";
						}else{
							$mode_suffix = "2";
						}
					}
				}
			}else{
				// if ($item->user_id == $current_user_id) {
				// 	$user = $room->user1;
				// } else {
					$user = $room->user2;
				// }
				//該商品尚未成交
				$bid = BI::queryLastBid( $item->id , $user);
				if ($bid != null) {
					$mode_suffix = "1";
				}else{
					$mode_suffix = "0";
				}
			}

			return $mode_pre . $mode_suffix;
		}

	}
}