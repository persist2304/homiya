<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		//如果是更新，就不需要檢查pic為必須
		if (Request::isMethod('PATCH')) 
		{
			return 
			[	
				'title'   => 'Alpha Dash|max:20',
				'pic'     => 'max:1100|mimes:jpeg,bmp,png',
				'price'   => 'Numeric',
				'city_id' => 'Numeric',
				// 'desc'    => 'Alpha Dash|max:300',
				'status'  => 'Numeric',
				'sort'    => 'Numeric',
			];
		}
		else
		{
			return 
			[
				'title'   => 'required|Alpha Dash|max:20',
				'pic'     => 'required|max:1100|mimes:jpeg,bmp,png',
				'price'   => 'required|Numeric',
				'city_id' => 'required|Numeric',
				// 'desc'    => 'required|Alpha Dash|max:300',
				'status'  => 'required|Numeric',
				'sort'    => 'required|Numeric',
			];
		}
	}

}
