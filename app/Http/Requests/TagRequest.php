<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class TagRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		//如果是交易行為，就不需要檢查pic為必須
		if (Request::input('mode') == 0) {
			//交易行為
			return 
			[
				'title'            => 'required|Alpha Dash|max:20',
				'sort'             => 'Numeric',
			];

		}else if(Request::input('mode') == 1){
			//分類
			//如果是更新，就不需要檢查pic為必須
			if (Request::isMethod('PATCH')) {
				return 
				[
					'pic'   => 'max:1100|mimes:jpeg,bmp,png',
					'title' => 'required|Alpha Dash|max:20',
					'sort'  => 'Numeric',
				];
			}else{
				return 
				[
					'pic'   => 'required|max:1100|mimes:jpeg,bmp,png',
					'title' => 'required|Alpha Dash|max:20',
					'sort'  => 'Numeric',
				];
			}
		}else if(Request::input('mode') == 2){
			//促銷活動
			return 
			[
				'title'            => 'required|Alpha Dash|max:20',
				'sort'             => 'Numeric',
			];		
		}else if(Request::input('mode') == 3){
			//促銷活動
			return 
			[
				'title'            => 'required|Alpha Dash|max:20',
				'sort'             => 'Numeric',
			];
		}

	}

}
