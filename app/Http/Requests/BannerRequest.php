<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannerRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		//如果是更新，就不需要檢查pic為必須
		if (Request::isMethod('PATCH')) 
		{
			return 
			[	
				'pic'   => 'max:1100|mimes:jpeg,bmp,png',
				//'target' => 'Alpha Dash|max:20',
				'url'   => 'url',
				'sort'   => 'Numeric',

			];
		}
		else
		{
			return 
			[
				'pic'   => 'required|max:1100|mimes:jpeg,bmp,png',
				//'target' => 'required|Alpha Dash|max:20',
				'url'   => 'url',
				'sort'   => 'required|Numeric',
			];
		}
	}

}
