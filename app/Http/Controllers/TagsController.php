<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//自行新增
use Request as urlRequest;
use App\Tag;
use App\Http\Requests\TagRequest;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Http\Model\PublicUtil;
use App\Http\Model\Constant;
use Input;
use Image;

class TagsController extends Controller 
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tags = Tag::all();
		//$tags = Tag::where('enabled','=','1')->get();
		$isNull = Constant::getS3Path();
		return view('tags.index',compact('tags','isNull'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tags.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(TagRequest $request)
	{
		$inputs = $request->all();
		//dd($inputs);

		//檢查Title是否有重複
		$oldTag = Tag::where('title',$inputs['title'])->first();
		if(isset($oldTag)){
			flash()->error('該標籤名稱已存在');
			return redirect('tags');
		}

		if(isset($inputs['pic'])){
			$file = PublicUtil::picUpload($request , "pic" , 600 , 360);
			if (isset($file)) 
			{
				flash()->success('新增成功');
				$tag = Tag::create($inputs);			
		      	$tag->pic = Constant::getS3Path().$file['pic'];
		      	$tag->save();
			}
			else
			{
				PublicUtil::delFile('uploads');
				flash()->error('新增失敗');
			}
			//因為需要進行本地壓縮，所以需要先存在本地，使用完後須刪除
			PublicUtil::delFile('uploads');
		}else{

			Tag::create($inputs);
		}
		
		return redirect('tags');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tag = Tag::findOrFail($id);
		$urlNow = urlRequest::url();
		return view('tags.edit', compact('tag','urlNow'));	
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id , TagRequest $request)
	{
		$inputs = $request->all();
		//檢查Title是否有重複
		$oldTag = Tag::where('title',$inputs['title'])->first();
		if(isset($oldTag) && $oldTag->id != $id){
			flash()->error('該標籤名稱已存在');
			return redirect('tags');
		}

		$tag = Tag::findOrFail($id);

		if(isset($inputs['pic'])){
			$file = PublicUtil::picUpload($request , "pic" , 600 , 360);
			if (isset($file)) 
			{			
		      	$tag->pic = Constant::getS3Path().$file['pic'];
		      	$tag->save();
		      	flash()->success('更新成功');
			}
			else
			{
				PublicUtil::delFile('uploads');
				flash()->error('更新失敗');
			}
			//因為需要進行本地壓縮，所以需要先存在本地，使用完後須刪除
			PublicUtil::delFile('uploads');
		}else{
			$tag->update($inputs);
		}

		return Redirect::to('tags');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	// //重複第三次了
	// private function tagsPicUpload(TagRequest $request)
	// {
	// // getting all of the post data
 //  	$files = [
 //  				'pic' => $request->file('pic'),
 //  			 ];
 //  	$isFail = false;
 //  	foreach ($files as $key => $value) 
 //  	{
 //  		if (isset($value)) 
 //  		{
 //  			if ($value->isValid()) 
 //  			{
 //    			$destinationPath = 'uploads'; // upload path
	//    			$extension = $value->getClientOriginalExtension(); // getting image extension
 //     			$fileName = PublicUtil::randomFileName(12) .'.' . $extension; // renameing image
 //     			Input::file('pic')->move($destinationPath, $fileName); // uploading file to given path
 //     			$data['filename'] = $fileName;//因為表格內沒有這個屬性，所以要自行新增
 //     			$img = Image::make('uploads/'.$data['filename'])->resize(350, 200)->save();//上傳後規定的大小
 //     			Storage::disk('s3')->put($fileName, ($img),'public');
 //      			$files[$key] = $fileName;
	//     	}
	//     	else
	//     	{
	//     		// sending back with error message.
	//     		$isFail = true;
	//    			$files[$key] = null;
	//    		}
 //  		}
 // 	}
 // 		if (!$isFail) 
 // 		{
 //  			return $files;
 //  		}
 //  		else
 //  		{
 //  			return null;
 //  		}
	// }

	//刪除uploads檔案
	// public function delFile($dirName)
	// {
	// 	if(file_exists($dirName) && $handle=opendir($dirName))
	// 	{
	// 		while(false!==($item = readdir($handle)))
	// 		{
	// 		if($item!= "." && $item != "..")
	// 		{
	// 			if(file_exists($dirName.'/'.$item) && is_dir($dirName.'/'.$item))
	// 			{
	// 				delFile($dirName.'/'.$item);
	// 			}
	// 			else
	// 			{
	// 				if(unlink($dirName.'/'.$item))
	// 				{
	// 					return true;
	// 				}
	// 			}
	// 		}
	// 	}
	// 		closedir( $handle);
	// 	}
	// }



}
