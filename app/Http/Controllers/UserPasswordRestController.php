<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\userPasswordRestRequest;
use App\User;
use App\Http\Model\BI;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\userPasswordRestTokenRequest;
use Carbon\Carbon;
use App\Password_reset;
use DB;

class UserPasswordRestController extends Controller 
{
	//取得忘記密碼頁面
	public function getEmail()
	{
		return view ('auth.password');
	}

	//驗證資料庫內是否有此信箱
	public function postEmail(userPasswordRestRequest $request)
	{
		// dd('start');
		$inputs = $request->all();
		$isUser = User::where('email',$inputs['email'])->first();
		// dd($isUser);
		if (is_null($isUser))
		{
			flash()->error('資料錯誤');
			return redirect(url('/login'));			
		}
		else
		{
			//計算目前時間
			$nowTime = Carbon::now()->format('Ymdhis');
			//組出token值
			$token = substr(hash('sha256', ('Homiya1' . $nowTime)) , 3 , 40);
			$inputs['token'] = $token;
			//寫入使用者資料進入資料庫
			$passwordReset = Password_reset::create($inputs);

			//資料庫內有資料後則發信給該使用者
			Mail::send('emails.linkPassword', ['token' => $token], function($message)
			{
			    $message->to($_POST["email"])->subject('重新設置密碼連結');
			});
//dd('show');
			flash()->success('已發送至Email');
			return view ('auth.send');
			//return redirect(url('/send'))->with('token','passwordReset');			
		}
	}

	//取得重設密碼頁面
	public function getReset(Request $request)
	{
		$token = $request->all()['token'];
		//dd($token);
		return view ('auth.userReset',compact('token'));
	}
	//驗證是否有token值，輸入兩次密碼是否一致
	public function postReset(userPasswordRestTokenRequest $request)
	{
		$inputs = $request->all();
		//dd($inputs['token']);
		$correctUser = Password_reset::where('email',$inputs['email'])->where('token',$inputs['token'])->first();
		if (is_null($correctUser))
		{
			return 'Erorrs';
		}
		else
		{			
			//Email不是亂輸的，接著要驗證Token
			if ($correctUser->token == $inputs["token"]) 
			{
				//驗證正確
			}
			else
			{
				return '未獲得變更密碼授權';
			}
			//進行資料變更階段
			//找出User資料表格內此使用者的資料，修改資料表格內的密碼
			$user = User::where('email',$inputs['email'])->first();
			$user->password = hash('sha256', $inputs['password']);
			$user->save();//存起來
			flash()->success('修改成功');
			DB::table('password_resets')->delete();
			return redirect(url('/login'));
		}
		DB::table('password_resets')->delete();
		return redirect(url('/login'));
	}
}