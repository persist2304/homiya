<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//自行新增
use Request as urlRequest;
use App\Banner;
use App\Http\Requests\BannerRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Model\PublicUtil;
use App\Http\Model\Constant;
use Input;
use Image;
use App\Tag;
use App\Item;
use App\User;
use Illuminate\Support\Facades\Storage;


class BannerController extends Controller 
{
	/*
		模式說明：
		1.mode為開啟模式(0：開啟網頁，1：某分頁的列表，2：某商品詳細頁面，3：某店家詳細頁面)
		2.target為當mode為1-3時所對應的資料，預設為id
		3.url為當model為0時所載入網址
	*/	
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		$banners = Banner::all();
		return view('banners.index',compact('banners'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$urlNow = urlRequest::url();
		$targetLists = Tag::where('id','!=','40')->lists('title','id');
		$itemsLists = Item::lists('title','id');
		$usersLists = User::lists('name','id');
		return view('banners.create',compact('urlNow','targetLists','itemsLists','usersLists'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(BannerRequest $request)
	{

		$inputs = $request->all();
		if($inputs["mode"] == 1){
			$inputs["target"]= $inputs["target_tag"];
		}else if($inputs["mode"] == 2){
			$inputs["target"]= $inputs["target_item"];
		}else if($inputs["mode"] == 3){
			$inputs["target"]= $inputs["target_user"];
		}else{

		}

		$file = PublicUtil::picUpload($request , 'pic' , 640 , 360);
		if (isset($file)) 
		{
			flash()->success('新增成功');
			$banner = Banner::create($inputs);
	      	$banner->pic = Constant::getS3Path().$file['pic'];
	      	$banner->save();
		}
		else
		{
			PublicUtil::delFile('uploads');
			flash()->error('新增失敗');
		}
		//因為需要進行本地壓縮，所以需要先存在本地，使用完後須刪除
		PublicUtil::delFile('uploads');
		return Redirect::to('banners');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$banner = Banner::findOrFail($id);
		$targetLists = Tag::lists('title','id');
		$itemsLists = Item::lists('title','id');
		$usersLists = User::lists('name','id');
		$urlNow = urlRequest::url();
		$target = $banner->target;
		return view('banners.edit', compact('banner','urlNow','targetLists','itemsLists','usersLists','target'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id , BannerRequest $request)
	{
		$inputs = $request->all();

		if($inputs["mode"] == 1){
			$inputs["target"]= $inputs["target_tag"];
		}else if($inputs["mode"] == 2){
			$inputs["target"]= $inputs["target_item"];
		}else if($inputs["mode"] == 3){
			$inputs["target"]= $inputs["target_user"];
		}else{

		}

		$banner = Banner::findOrFail($id);
		$file = PublicUtil::picUpload($request);
		// dd($file);
		if (isset($file)) 
		{
			flash()->success('更新成功');
	      	
			if ($file['pic'] == null) 
			{
				if (isset($banner->pic)) 
				{
					$inputs['pic'] = $banner->pic;
				}
			}
			else
			{
				PublicUtil::delFile('uploads');
				$inputs['pic'] = Constant::getS3Path().$file['pic'];
			}
			$banner->update($inputs);
  		}
  		PublicUtil::delFile('uploads');
		return redirect('/banners');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
