<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//自行新增
use Request as urlRequest;
use App\Item;
use App\Http\Requests\ItemRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Model\PublicUtil;
use Input;
use Image;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Tag;
use App\Http\Model\BI;

class ItemsController extends Controller 
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Item::where('enabled','=','1')->get();
		$itemSuccessful = BI::sellOut();
		$urlNow = urlRequest::url();
		return view('items.index',compact('items','itemSuccessful','urlNow'));
	}

	public function index_down()
	{
		$items_down = Item::where('enabled','=','0')->get();
		//dd($items_down);
		$itemSuccessful = BI::sellOut();
		$urlNow = urlRequest::url();
		return view('items.index_down',compact('items_down','itemSuccessful','urlNow'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$urlNow = urlRequest::url();
		$targetLists = Tag::lists('title','id');
		return view('items.create',compact('urlNow','targetLists'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ItemRequest $request)
	{
		// $inputs = $request->all();
		// $files = $this->itemsPicUpload($request);
		// if (isset($files)) 
		// {
		// 	$items = Item::create($inputs);
	 //      	$items->pic = $files['pic'];
	 //      	$items->save();
		// }
		// else
		// {
		// 	PublicUtil::delFile('uploads');
		// 	return 'error';
		// }

		// $this->delFile('uploads');
		// return Redirect::to('items');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = Item::findOrFail($id);
			if (isset($item->pic1) )
			{
				$high = '500';
				$width = '500';
			}
				if (isset($item->pic1) && isset($item->pic2) )
			{
				$high = '500';
				$width = '250';
			}
		if (isset($item->pic1) && isset($item->pic2) && isset($item->pic3) )
			{
				$high = '250';
				$width = '250';
				$width3 = '500';
			}
		if (isset($item->pic1) && isset($item->pic2) && isset($item->pic3) && isset($item->pic4))
			{
				$high = '250';
				$width = '250';
			}
		
	//dd($high,$width);

		$userIds = User::lists('name','id');
		$urlNow = urlRequest::url();
		//dd($urlNow);
		$targetLists = Tag::lists('title','id');
		return view('items.edit', compact('item','urlNow','userIds','targetLists','high','width','width3'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id , ItemRequest $request)
	{
		$item = Item::findOrFail($id);
		$file = PublicUtil::picUpload($request);
		// dd($file);
		if (isset($file)) 
		{
	      	$inputs = $request->all();
			if ($file['pic'] == null) 
			{
				if (isset($item->pic)) 
				{
					$inputs['pic'] = $item->pic;
				}
			}
			else
			{
				PublicUtil::delFile('uploads');
				$inputs['pic'] = $file['pic'];
			}
			$item->update($inputs);
  		}
  		PublicUtil::delFile('uploads');
  
		return redirect('/items');
			
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	//與Banner上傳功能類似，重新寫的原因在於?
	// private function itemsPicUpload(ItemRequest $request)
	// {
	// // getting all of the post data
 //  	$files = [
 //  				'pic' => $request->file('pic'),
 //  			 ];
 //  	$isFail = false;
 //  	foreach ($files as $key => $value) 
 //  	{
 //  		if (isset($value)) 
 //  		{
 //  			if ($value->isValid()) 
 //  			{
 //    			$destinationPath = 'uploads'; // upload path
	//    			$extension = $value->getClientOriginalExtension(); // getting image extension
 //     			$fileName = PublicUtil::randomFileName(12) .'.' . $extension; // renameing image
 //     			Input::file('pic')->move($destinationPath, $fileName); // uploading file to given path
 //     			$data['filename'] = $fileName;//因為表格內沒有這個屬性，所以要自行新增
 //     			$img = Image::make('uploads/'.$data['filename'])->resize(350, 200)->save();//上傳後規定的大小
 //     			Storage::disk('s3')->put($fileName, ($img),'public');
 //      			$files[$key] = $fileName;
	//     	}
	//     	else
	//     	{
	//     		// sending back with error message.
	//     		$isFail = true;
	//    			$files[$key] = null;
	//    		}
 //  		}
 // 	}
 // 		if (!$isFail) 
 // 		{
 //  			return $files;
 //  		}
 //  		else
 //  		{
 //  			return null;
 //  		}
	// }


	// //與Banner同名功能類似
	// public function delFile($dirName)
	// {
	// 	if(file_exists($dirName) && $handle=opendir($dirName))
	// 	{
	// 		while(false!==($item = readdir($handle)))
	// 		{
	// 		if($item!= "." && $item != "..")
	// 		{
	// 			if(file_exists($dirName.'/'.$item) && is_dir($dirName.'/'.$item))
	// 			{
	// 				delFile($dirName.'/'.$item);
	// 			}
	// 			else
	// 			{
	// 				if(unlink($dirName.'/'.$item))
	// 				{
	// 					return true;
	// 				}
	// 			}
	// 		}
	// 	}
	// 		closedir( $handle);
	// 	}
	// }

	//此功能應移到BI裡去
	// public function countSuccessful()
	// {
	// 	$itemsSuccessful = Item::where('status','=','1')->get();
	// 	return view('items.index',compact('itemsSuccessful'));
	// }
}
