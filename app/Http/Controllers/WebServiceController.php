<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BannerController;
use Validator;
use Response;
use App;
use DB;
use Lang;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Token;
use App\User;
use App\Device;
use App\Country;
use App\Zone;
use App\City;
use App\Item;
use App\Track;
use App\Comment;
use App\Banner;
use App\Tag;
use App\Follow;
use App\Evaluate;
use App\Message;
use App\Room;
use App\Password_reset;
use Input;
use Image;
use Illuminate\Support\Facades\Storage;
//Modal
use App\Http\Model\PublicUtil;
use App\Http\Model\BI;
use App\Http\Model\Constant;


class WebServiceController extends Controller {

	public function show() {
		// $ip = App::environment('local') ? 'localhost' : '52.35.133.107';
		$ip = Constant::showIp();
		// $local = App::environment('local') ? 'homiyaLocal' : 'homiya';
		$local = Constant::localFolder();
		return view('webService.show', compact('ip', 'local'));
	}

	/*
	 * fun: device/token 
	 * @return Content token
	 */
	public function getToken(){
		//生成一隨機id，進行sha256編碼後，取前六位元
		$str_token = substr(hash('sha256', (uniqid(rand(), true))) , 0 , 6);
		$token = Token::create(['token'=>$str_token]);
		if (isset($token)) {
			return $this->makeWsFormat(['token'=>$token->token], 1 , null);
		}else{
			return $this->makeWsFormat( null , 0 , '建立Token失敗');
		}
	}

	/*
	 * fun: device/register
	 * @param token
	 * @param deviceType
	 * @param deviceCode
	 * @param check
	 * @return salt
	 */
	public function registerDevice(Request $request){
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'token',null,null,null);
		$this->checkParam($errors ,$request ,'deviceType',null,['ios','android'],null);
		if (count($errors) > 0) {
			return $errors[0];
		}
		
		$inputs = $request->all();
		//dd($inputs);
		
		if (App::environment('dev') || App::environment('local')){
			$updateTime = 12000000;	
		}else {
			$updateTime = 120;
		}

		//token檢查
		if (! isset($inputs['token'])) {
			return $this->makeWsFormat( null , 0  , '缺少parameter:token');
		}else{
			$token = Token::where('token' , $inputs['token'])->first();
			if (!isset($token)) {
				return $this->makeWsFormat( null , 0  , 'token不正確');
			}
		}

		if ($token->created_at->diffInSeconds(Carbon::now() , true) > $updateTime) {
			$token->delete();
			return $this->makeWsFormat( null , 0  , 'token已過期');
		}

		try {
			//先檢查該device是否已經註冊
			$device = Device::where('deviceCode' , $inputs['deviceCode'])->first();
			if (isset($device)) {
				return $this->makeWsFormat(['salt'=>$device->salt], 1 , null);
			}else{
				//生成Salt碼，規則為HOMIYA1 + deviceType + deviceCode + 當前時間 ，sha256後取前十位
				$salt = substr(hash('sha256', ('HOMIYA1' . $inputs['deviceType'] . $inputs['deviceCode'] . Carbon::now())),0 , 10);
				//dd('salt:' . $salt );
				$device = Device::create(['deviceType'=>$inputs['deviceType'],'deviceCode'=>$inputs['deviceCode'],'salt'=>$salt]);
				return $this->makeWsFormat(['salt'=>$salt], 1 , null);
			}
			
		} catch (\Illuminate\Database\QueryException $e) {
			return $this->makeWsFormat(null , 0 , trans('device.unregister'));
		}
		
	}

	/*
	 * fun: user/normalLogin
	 * @param check
	 * @param email
	 * @param password
	 * @return 
	 */
	public function normalLogin(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'email',null,null,'email');
		$this->checkParam($errors ,$request ,'password',null,null,null);
		if (count($errors) > 0) {
			return $errors[0];
		}

		$inputs = $request->all();
		//dd($inputs['password']);
        $user = User::where('email', $inputs['email'])->where('enabled',1)->first();
        if ($user != null) {
        	//$isCorrect = $user->password === hash('sha256', $inputs['password']);
        	$isCorrect = $user->password == $inputs['password'];
        	if ($isCorrect) {
        		//登入成功
        		$user->lastLogin = Carbon::now();
        		$user->save();
        		//加入必要的額外資料
        		if($user->city){
        			$user["city_name"] = $user->city->title;
					$user['zone_name'] = $user->city->zone->title;
        		}
				$user['likeNames'] = PublicUtil::transferTagIdsToTagTitles($user->likes);
				//轉化欄位資料
				if (isset($user->birthday)){
					$user['birth'] = $user->birthday->format("d/m/Y");
				}
        		return $this->makeWsFormat($user , 1 , null);
        	}else{
        		return $this->makeWsFormat( null, 0 , trans('messages.loginFail'));
        	}
        }else{
        	return $this->makeWsFormat( null, 0 , trans('messages.loginFail'));
        }
	}

	/*
	 * fun: user/register
	 * @param check
	 * @param name
	 * @param email
	 * @param password
	 * @param gender
	 * @param pic
	 * @return 
	 */
	public function normalRegister(Request $request) {
		$errors = array();
		$inputs = $request->all();
		//dd($inputs);
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'name',30,null,null);
		$this->checkParam($errors ,$request ,'email',null,null,'email');
		$this->checkParam($errors ,$request ,'password',null,null,null);
		//$this->checkParam($errors ,$request ,'gender',null,['male','female','neutral'],null);
		//$this->checkParam($errors ,$request ,'pic',null,null,null);
		if (count($errors) > 0) {
			return $errors[0];
		}


		//檢查該Email是否有重複註冊情況
		$userByEmail = User::where('email' , $inputs['email'])->first();
		if (isset($userByEmail)) {
			//該Email已經存在
			return $this->makeWsFormat(null , 0 , trans('messages.emailExist'));
		}
		if (isset($inputs['pic'])){
	        $file = PublicUtil::picUpload($request , 'pic' , 300 , 300);
	        if (isset($file)) {
		      	$pic = Constant::getS3Path().$file['pic'];
		      	$inputs['pic'] = $pic;
			} else {
				return $this->makeWsFormat( null , 0 , trans('messages.imageUploadFail'));
			}
		}
		else //若為空預設給空白照
		{
			$inputs['pic'] = 'https://s3-us-west-2.amazonaws.com/schoolpic/pw978k1wx1qg.jpg';
		}
		if (!isset($inputs['gender'])){ //若為空預設給male
			$inputs['gender'] = 'male';
		}
		//$password = hash('sha256', ($inputs['password']));
		$password = $inputs['password'];
		$desc = Constant::getDefaultDesc();
        $newUser = User::create([
        	'name'=>$inputs['name'],
        	'username'=>$inputs['name'],
        	'email'=>$inputs['email'], 
        	'password'=>$password,
        	'gender'=>$inputs['gender'],
        	'pic'=>$inputs['pic'],
        	//'pic'=>$pic,
        	'desc'=>$desc,
        	'lastLogin'=>Carbon::now()]);
        if (isset($inputs['pic'])){
        	PublicUtil::delFile('uploads');
   		}
        if (isset($newUser)) {
        	return $this->makeWsFormat($newUser , 1 , null);
        }else{
        	return $this->makeWsFormat(null , 0 , trans('messages.registerFail'));
        }
	}

	/*
	 * fun: user/fbLogin
	 * @param check
	 * @param username
	 * @param name
	 * @param email (option)
	 * @param gender
	 * @param locale
	 * @param birthday
	 * @param fb_id
	 * @return 
	 */
	public function fbLogin(Request $request) {
		$result = true;
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'name',30,null,null);
		//$this->checkParam($errors ,$request ,'email',null,null,'email');
		$this->checkParam($errors ,$request ,'gender',null,['male','female','neutral'],null);
		//$this->checkParam($errors ,$request ,'locale',null,null,null);
		//$this->checkParam($errors ,$request ,'birthday',null,null,null);
		$this->checkParam($errors ,$request ,'fb_id',null,null,null);
		if (count($errors) > 0) {
			return $errors[0];
		}

		$inputs = $request->all();
		//dd($inputs);

		//解決可能沒有Email欄位的問題
		if (!isset($inputs['email'])) {
			$inputs['email'] = null;
		}

		try {
			//先檢查是否有相同Email的用戶
			$user_email = User::where('email', $inputs['email'])->first();
			//先檢查是否有相同Fb_id的用戶
			$user = User::where('fb_id', $inputs['fb_id'])->first();

			


			if (isset($user)) {
				//如果該使用者被停用則無法進行登入
				if ($user->enabled == 0) {
					return $this->makeWsFormat( null, 0 , trans('messages.userDisabled'));
				}

				// //如果該用戶存在但fb_id欄未輸入則補上
				// if ($user->fb_id == null) {
				// 	$user->fb_id = $inputs['fb_id'];
				// 	$user->name = $inputs['name'];
				// 	$user->firstName = $inputs['first_name'];
				// 	$user->lastName = $inputs['last_name'];
				// }else{
				// 	//如果fb_id欄不同則更新之
				// 	if ($user->fb_id != $inputs['fb_id']) {
				// 		$user->fb_id = $inputs['fb_id'];
				// 	}
				// }
			}else{
				//確認email是否唯一
				if(isset($user_email)){
					return $this->makeWsFormat( null, 0 , trans('messages.userDouble'));
				}	
				//如果該用戶不存在則進行建立
				$pic = PublicUtil::buildFbPicPath($inputs['fb_id']);
				$user = User::create(['username'=>$inputs['name'],
					'name'=>$inputs['name'],
					'email'=>$inputs['email'],
					'gender'=>$inputs['gender'],
				    'fb_id'=>$inputs['fb_id'],
				    'firstName'=>$inputs['first_name'],
				    'lastName'=>$inputs['last_name'],
				    'pic'=>$pic,
				    'desc'=>Constant::getDefaultDesc(),
				    'lastLogin'=>Carbon::now()]);
			}
		
		} catch (\Illuminate\Database\QueryException $e) {
			//dd($e);
			return $this->makeWsFormat(null , 1 , trans('messages.databaseError'));
			$fbUser = User::where('email', $inputs['email'])->first();
			return $this->makeWsFormat($fbUser , 1 , null);

		}
		if (isset($user)) {
			$user->lastLogin = Carbon::now();
			$user->save(); //將變更資料存進資料庫
			//加入必要的額外資料
			if(isset($user->city)){
				$user["city_name"] = $user->city->title;
				$user['zone_name'] = $user->city->zone->title;
			}
			$user['likeNames'] = PublicUtil::transferTagIdsToTagTitles($user->likes);
			//轉化欄位資料
			if (isset($user->birthday)){
				$user['birth'] = $user->birthday->format("d/m/Y");
			}
			return $this->makeWsFormat($user , 1 , null);
		}else{
			return $this->makeWsFormat(null , 0 , trans('messages.fbLoginError'));
		}
		
		
	}

	/*
	 * fun: user/autoLogin
	 * @param check
	 * @param user_id,fb_id (option), email(option) 擇一 
	 * @return 
	 */
	public function autoLogin(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,'integer');
		if (count($errors) > 0) {
			return $errors[0];
		}

		$inputs = $request->all();
		if(!isset($inputs['fb_id']) && !isset($inputs['email'])){
			return $this->makeWsFormat( null, 0 , trans('messages.loginDataNotEnough'));
		}
		if(isset($inputs['fb_id'])){
			$user = User::where('fb_id', $inputs['fb_id'])->where('id',$inputs['user_id'])->where('enabled',1)->first();
		}else if(isset($inputs['email'])){
			$user = User::where('email', $inputs['email'])->where('id',$inputs['user_id'])->where('enabled',1)->first();
		}
        if ($user != null) {
        	$user->lastLogin = Carbon::now();
        	$user->save();
        	//加入必要的額外資料
        	if(isset($user->city)){
        		$user["city_name"] = $user->city->title;
				$user['zone_name'] = $user->city->zone->title;
        	}
			$user['likeNames'] = PublicUtil::transferTagIdsToTagTitles($user->likes);
			//轉化欄位資料
			if (isset($user->birthday)){
				$user['birth'] = $user->birthday->format("d/m/Y");
			}
        	return $this->makeWsFormat($user , 1 , null);
        }else{
        	return $this->makeWsFormat( null, 0 , trans('messages.loginFail'));
        }
	}

	/*
	 * fun: user/changePwd
	 * @param check
	 * @param user_id
	 * @param email unused
	 * @param oldPwd option
	 * @param newPwd
	 * @param emailChk unused
	 */
	public function changePwd(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		//$this->checkParam($errors ,$request ,'oldPwd',null,null,null); //如果從未輸入過，可以不提供
		$this->checkParam($errors ,$request ,'newPwd',null,null,null);
		//$this->checkParam($errors ,$request ,'email',null,null,'email');
		//$this->checkParam($errors ,$request ,'emailChk',null,null,null);
		
		if (count($errors) > 0) {
			return $errors[0];
		}
		
		$inputs = $request->all();

		$user = User::find($inputs['user_id']);
		if(isset($user)){
			//如果已有舊密碼就必須提供
			if(isset($user->password)){
				if(!isset($inputs['oldPwd'])){
					return $this->makeWsFormat( null, 0 , trans('messages.oldPasswordNotHave'));
				}	
			}
		}else{
			return $this->makeWsFormat( null, 0 , trans('messages.userNotExist'));
		}

		if (isset($user->password)){
			if($user->password != $inputs["oldPwd"]){
				return $this->makeWsFormat(null, 0, trans('messages.oldPasswordNotEqual'));
			}
		}

		$user->update(['password'=>$inputs['newPwd']]);
		return $this->makeWsFormat(null, 1, null);

		//安全模式版本
		//先檢查密碼變更者身份
		// $pwdReset = Password_reset::where('email',$inputs['email'])->where('token',$inputs['emailChk'])->first();
		// if (isset($pwdReset)) {
		// 	$user = User::where('id', $inputs['user_id'])->first();
		// 	//如果該使用者被停用則無法進行操作
		// 	if ($user->enabled == 0) {
		// 		return $this->makeWsFormat( null, 0 , trans('messages.userDisabled'));
		// 	}

		// 	$oldHashPwd = hash('sha256', $inputs['oldPwd']);
		// 	if ($user->password === $oldHashPwd) {
		// 		//$user->password = hash('sha256', $inputs['newPwd']);
		// 		$user->update(['password'=>$inputs['newPwd']]);
		// 		$pwdReset->delete();
		// 		return $this->makeWsFormat(null, 1, null);
		// 	} else {
		// 		return $this->makeWsFormat(null, 0, trans('messages.oldPasswordNotEqual'));
		// 	}
		// }else{
		// 	return $this->makeWsFormat(null, 0, trans('messages.emailChkWrong'));
		// }
	}

	/*
	 * fun: user/getData
	 * @param check
	 * @param user_id
	 * @param current_user_id
	 * @return Dictionary []
	 */
	public function getUserData(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		//$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'user_id',null,null);//若沒登入了話
		//$this->checkParam($errors ,$request ,'current_user_id',null,null,"integer");//若沒登入了話
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$user = User::where( 'id' , $inputs['user_id'])->where( 'enabled' , 1 )->first();
		
		if (!isset($user)) {
			return $this->makeWsFormat( null, 0, "找不到該用戶資料");
		}

		$user['signup_at'] = $user['created_at']->diffForHumans();
		$user['signup_date'] = PublicUtil::carbonToDate($user['created_at']);

		//$birthday = Carbon::parse($user['birthday']);
		//$user['birthday'] = PublicUtil::carbonToDate($birthday);
		$fans_qty = Follow::where('user_id', $inputs['user_id'])->count();
		$user['fans_qty'] = $fans_qty;
		$tracks_qty = Follow::where('fan_id', $inputs['user_id'])->count();
		$user['tracks_qty'] = $tracks_qty;
		$items_qty = Item::where('user_id', $inputs['user_id'])->where('enabled', 1)->count();
		$user['items_count'] = $items_qty;
		$user['value_good'] = BI::queryEvaluate($inputs['user_id'], '1');
		$user['value_normal'] = BI::queryEvaluate($inputs['user_id'], '2');
		$user['value_bad'] = BI::queryEvaluate($inputs['user_id'], '3');
		//轉化欄位資料
		if (isset($user->birthday)){
			$user['birth'] = $user->birthday->format("m/d/Y");
		}
		//若沒登入了話
	if(isset($inputs['current_user_id'])){//若沒登入了話
		$isTrack = Follow::where('fan_id', $inputs['current_user_id'])->where('user_id', $inputs['user_id'])->first();
	}
	else{
		$isTrack = null;
	}

		$user['isTrack'] = $isTrack == null ? false : true;
		if (isset($user['city_id'])) {
			$city = $user->city;
			$zone = $city->zone;
			$user['city_name'] = $city->title;
			$user['zone_name'] = $zone->title;
		}
		
		return $this->makeWsFormat($user, 1, null);	
	}

	/*
	 * fun: user/update
	 * @param check
	 * @param user_id
	 * @param name
	 * @param city_id
	 * @param email
	 * @param mobile
	 * @param gender
	 * @param firstName(option)
	 * @param lastName(option)
	 * @param desc(option)
	 * @param pic(option)
	 * @param birthday(option)
	 * @param likes(option)
	 * @param website(option)
	 * @return Dictionary []
	 */
	public function updateUserData(Request $request) {
		$result = true;
		$errors = array();
		$inputs = $request->all();

		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'name',30,null,null);
		$this->checkParam($errors ,$request ,'city_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'email',null,null,'email');
		$this->checkParam($errors ,$request ,'mobile',null,null,null);
		$this->checkParam($errors ,$request ,'gender',null,['male','female','neutral'],null);
		if (count($errors) > 0) {
			return $errors[0];
		}
		

		if (!isset($inputs['firstName'])) {
			$inputs['firstName'] = null;
		}

		if (!isset($inputs['lastName'])) {
			$inputs['lastName'] = null;
		}

		if (!isset($inputs['desc'])) {
			$inputs['desc'] = null;
		}

		if (!isset($inputs['birthday']) || strlen(trim($inputs['birthday']))==0) {
			$inputs['birthday'] = null;
		} else {
			$inputs['birthday'] = date_create_from_format('m/d/Y', $inputs['birthday']);
		}

		if (!isset($inputs['likes']) || strlen(trim($inputs['likes']))==0) {
			$inputs['likes'] = null;
		}

		if (isset($inputs['pic'])) {
			$file = PublicUtil::picUpload($request, "pic", 300, 300);
		    if (isset($file)) {
			    $pic = Constant::getS3Path().$file['pic'];
			    $inputs['pic'] = $pic;
			} else {
				return $this->makeWsFormat('圖片上傳失敗' , 0 , null);
			}
		}
		$user = User::where( 'id' , $inputs['user_id'])->where( 'enabled' , 1 )->first();
		if (isset($user)) {
			$user->update($inputs);
		}else{
			$result = false;
		}
		
		PublicUtil::delFile('uploads');
		if ($result == true) {
			return $this->makeWsFormat(['pic' => $user->pic], 1, null);
		}else{
			return $this->makeWsFormat(null, 0, trans('messages.userUppdateFail'));
		}
	}

	/*
	 * fun: banner/getList
	 * @param check
	 * @return Dictionary []
	 */
	public function getBannerList(Request $request) {
		// $errors = array();
		// $this->basicCheck($errors ,$request);
		// if (count($errors) > 0) {
		// 	return $errors[0];
		// }
		$banners = Banner::where( 'enabled' , 1 )->orderBy('sort','asc')->get();
		if (isset($banners) && count($banners) > 0) {
			for ($i=0; $i < count($banners) ; $i++) { 
				if($banners[$i]->mode == 1){
					$banners[$i]->tagTitle = Tag::find($banners[$i]->target)->title;
				}
			}
			return $this->makeWsFormat(['banners'=>$banners], 1, null);
		}else{
			return $this->makeWsFormat( null, 0, "找不到廣告資料");
		}
	}

	/*
	 * fun: banner/click
	 * @param check
	 * @param user_id
	 * @param banner_id
	 */
	public function clickBanner(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'banner_id',null,null,"integer");

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$user = User::find($inputs['user_id']);
		if (!isset($user)) {
			return $this->makeWsFormat(null, 0, "找不到該使用者");
		}
		
		$banner = Banner::where('id', $inputs['banner_id'])->first();
		if (isset($banner)) {
			$banner->clicks = $banner->clicks + 1;
			$banner->save();
			return $this->makeWsFormat($banner, 1, null);
		}else{
			return $this->makeWsFormat(null, 0, "找不到該Banner");
		}
	}

	/*
	 * fun: tag/getList
	 * @param check
	 * @param user_id(option) 用以將該用戶的喜好標籤往前排，尚未開發
	 * @param mode(option)
	 * @return Array
	 */
	public function getTagList(Request $request) {
		// $errors = array();
		// $this->basicCheck($errors ,$request);
		// if (count($errors) > 0) {
		// 	return $errors[0];
		// }
		$inputs = $request->all();
		//dd($inputs);
		if (isset($inputs['mode'])) {
			$this->checkParam($errors ,$request , 'mode' , null , [0,1,2] , null);
			$tags = Tag::where('mode', $inputs['mode'])->where('enabled',1)->orderBy( 'sort' , 'asc')->get();
		} else {
			$tags = Tag::where('enabled',1)->orderBy( 'sort' , 'asc')->get();
		}
		if ( isset($tags) && count($tags) > 0) {
			return $this->makeWsFormat(['tags'=>$tags], 1, null);
		}else{
			return $this->makeWsFormat( null, 0, "找不到標籤資料");
		}
	}

	/*
	 * fun: country/getList
	 * @param check
	 * @return Array Dictionary [ 'country_id' , 'title' , 'sort' ]
	 */
	public function getCountryList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		if (count($errors) > 0) {
			return $errors[0];
		}
		$countries = Country::where('enabled',1)->get();
		if (isset($countries) && count($countries) > 0) {
			return $this->makeWsFormat(['countries'=>$countries], 1, null);
		}else{
			return $this->makeWsFormat( null, 0, "找不到國家資料");
		}
	}

	/*
	 * fun: zone/getList
	 * @param check
	 * @param country_id
	 * @return Array Dictionary [ 'zone_id' , 'title' , 'sort' ]
	 */
	public function getZoneList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request , 'zone/getList');
		$this->checkParam($errors ,$request ,'country_id',null,null,"integer");

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();
		$zones = Zone::where('country_id' , $inputs['country_id'])->where('enabled',1)->get();
		if (isset($zones) && count($zones) > 0) {
			//dd('zone count:' . count($zones));
			return $this->makeWsFormat(['zones'=>$zones], 1, null);
		}else{
			return $this->makeWsFormat( null, 0, "找不到行政區資料");
		}
	}

	/*
	 * fun: city/getList
	 * @param check
	 * @param zone_id
	 * @return Array Dictionary [ 'city_id' , 'title' , 'sort' ]
	 */
	public function getCityList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'zone_id',null,null,"integer");

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$zone = Zone::find($inputs['zone_id']);
		if (!isset($zone)) {
			return $this->makeWsFormat( null, 0, "該行政區不存在");
		}

		$cities = City::where('zone_id' , $zone->id)->where('enabled',1)->get();
		if (isset($cities) && count($cities) > 0) {
			return $this->makeWsFormat(['cities'=>$cities], 1, null);
		}else{
			return $this->makeWsFormat( null, 0, "找不到城市資料");
		}
		
	}

	/*
	 * fun: item/getList 此功能距離排序未完成，暫不提供。預設都是以生成時間進行排序
	 * @param check
	 * @param city_id
	 * @param current_user_id
	 * @param trackingMode
	 * @param isBid(option)
	 * @param sortMode(option)
	 * @param ownUser_id(option)
	 * @param tags(option)
	 * @param ownUser_id(option)
	 * @param keyword(option)
	 * @param priceMax(option)
	 * @param priceMin(option)
	 * @param location(option)
	 * @return Array Dictionary [ 'item_id' , 'title' , 'pic1' , 'price', 'city_id', 'city_id', 'user_id', 'available_at', 'track_qty', 'comment_qty', 'sort']
	 */
	public function getItemList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'city_id',null,null,"integer");
		//$this->checkParam($errors ,$request ,'current_user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'trackingMode',null,null,"boolean");
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();
	if(isset($inputs['current_user_id'])){ //若沒登入了話
		$currentUser = User::find($inputs['current_user_id']);
		if (!isset($currentUser)) {
			return $this->makeWsFormat(null, 0, "找不到該用戶資料");
		}
	}
		if (isset($inputs['isBid']) && $inputs['isBid'] == 'true' && isset($currentUser)){//若沒登入了話
			//查詢某個用戶出價過的商品，並以出價時間進行排序
			$sql = "select m.item_id from messages m where fromUser_id=" . $inputs['current_user_id'] . " and mode = 1 and created_at=(select max(created_at) from messages where item_id=m.item_id) order by created_at desc"; //一旦取消出價就不算出價商品了
			//$sql = "select distinct item_id from messages where fromUser_id = " . $inputs['current_user_id'] ." and mode = 1 order by created_at desc";
			$item_ids = DB::select($sql);
			//dd(count($item_ids));
			$items = array();
			for($i = 0 ; $i < count($item_ids) ; $i++){
				$item = Item::find($item_ids[$i]->item_id);
				if ($item->enabled == 1) {
					$items[] = $item;
				}
			}

		}elseif (isset($inputs['ownUser_id'])) {
			//查詢屬於某個用戶的商品，並以生成時間進行排序
			$ownUser = User::find($inputs['ownUser_id']);
			if (!isset($ownUser)) {
				return $this->makeWsFormat(null, 0, "找不到該用戶資料");
			}
			$items = Item::where('user_id', $inputs['ownUser_id'])->where('enabled',1)->orderBy('created_at', 'desc')->get();
			//dd(count($items));
		}elseif($inputs['trackingMode'] == 'true'){
			//查詢某個用戶所追蹤的商品，並以生成時間進行排序
			//dd('查詢追蹤商品');
			$items = Item::whereIn('id', function($query) use($currentUser){
		    $query->select('item_id')
		    ->from(with(new Track)->getTable())
		    ->where('user_id', $currentUser->id);})->where('enabled',1)->orderBy('created_at','desc')->get();
		    //dd(count($items));
		}else{
			$this->checkParam($errors ,$request ,'sortMode',null,null,null);
			//於商品列表針對所提供條件進行過濾與排序
			if ($inputs['sortMode'] == 0) {
				//以熱門商品來排序，即商品點擊率
				$items = PublicUtil::returnPricedItemsWithSorted($inputs, 'clicks', 'desc');
			} else if ($inputs['sortMode'] == 1) {
				//以人氣賣家來排序，即用戶被追蹤數排序
				$items = PublicUtil::returnPricedItemsWithSorted($inputs, 'user_rank', 'desc');
			} else if ($inputs['sortMode'] == 2) {
				//以商品的上架時間來進行排序
				$items = PublicUtil::returnPricedItemsWithSorted($inputs, 'created_at', 'desc');
			} else if ($inputs['sortMode'] == 3) {
				//價格由低到高排序
				$items = PublicUtil::returnPricedItemsWithSorted($inputs, 'price', 'asc');
			} else if ($inputs['sortMode'] == 4) {
				//價格由高到低排序
				$items = PublicUtil::returnPricedItemsWithSorted($inputs, 'price', 'desc');
			} else if ($inputs['sortMode'] == 5) {
				//距離最近的，未完成...
				$items = PublicUtil::returnPricedItemsWithSorted($inputs, 'distance', 'desc');
			}
		}

		if (!isset($items) || count($items) == 0) {
			return $this->makeWsFormat(['items'=>[]], 1, null);
		}else{
			//dd($items[1]);
			for ($i = 0; $i < count($items); $i++) {
				$track_qty = count(Track::where('item_id', $items[$i]->id)->get());
				$comment_qty = count(Comment::where('item_id', $items[$i]->id)->get());
				//dd(count($comment_qty));
				$user = User::where('id', $items[$i]->user_id)->first();
				//dd($user->id);
				if (!isset($user)) {
					dd('該使用者並不存在');
				}
	if(isset($inputs['current_user_id'])){//若沒登入了話
				$track = Track::where('user_id', $inputs['current_user_id'])->where('item_id', $items[$i]->id)->first();
	}
	else{
		$track = null;//若沒登入了話
	}
				$created_at_date = Carbon::createFromFormat('Y-m-d H:i:s', $items[$i]->created_at);
				$items[$i]->available_at = $created_at_date->diffForHumans();
				$items[$i]->track_qty = $track_qty;
				$items[$i]->comment_qty = $comment_qty;
				//dd($user->name);

				$items[$i]->user_name = $user->name;
				//dd($items[$i]);
				$items[$i]->user_pic = $user->pic;
				$items[$i]->isTrack = $track == null ? false : true;
			}
			return $this->makeWsFormat(['items'=>$items], 1, null);
		}
	}

	/*
	 * fun: item/upload
	 * @param check
	 * @param pic1
	 * @param tags
	 * @param qty
	 * @param user_id
	 * @param city_id
	 * @param title
	 * @param price
	 * @param desc(option)
	 * @param pic2(option)
	 * @param pic3(option)
	 * @param pic4(option)
	 */
	public function uploadItem(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'pic1',null,null,null);
		$this->checkParam($errors ,$request ,'tags',null,null,null);
		$this->checkParam($errors ,$request , 'qty' ,null,null,"integer");
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'city_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'title',null,null,null);
		$this->checkParam($errors ,$request ,'price',null,null,"integer");
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		if (!isset($inputs['pic2'])) {
			$inputs['pic2'] = null;
		}
		if (!isset($inputs['pic3'])) {
			$inputs['pic3'] = null;
		}
		if (!isset($inputs['pic4'])) {
			$inputs['pic4'] = null;
		}

		if (!isset($inputs['desc'])) {
			$inputs['desc'] = Constant::getDefaultItemDesc();
		}

		$inputs['sort'] = Constant::getDefaultSort();
		$inputs['enabled'] = Constant::getDefaultItemEnabled();
		$item = Item::create($inputs);

		//製作商品標籤字串
		//$item['tags'] = PublicUtil::buildTagString($inputs['tags']); 前台經處理好

		//處理圖片
		for ($i=1; $i <5 ; $i++) { 
			$picName = 'pic' . $i;
			if (isset($inputs[$picName])) {
				$files = PublicUtil::picUpload($request , $picName , 600 , 600);
			    if (isset($files)) {
				    $pic = Constant::getS3Path().$files[$picName];
				    $item[$picName] = $pic;
				} else {
					return $this->makeWsFormat( null , 0 , trans('messages.imageNameUploadFail',['id',$i]));
				}
			}
		}
		
		PublicUtil::delFile('uploads');
		$item->save();
		return $this->makeWsFormat($item, 1, null);
	}

	/*
	 * fun: item/update
	 * @param check
	 * @param qty
	 * @param tags
	 * @param item_id
	 * @param city_id
	 * @param title
	 * @param price
	 * @param status
	 * @param isDelete
	 * @param pic1(option)
	 * @param desc(option)
	 * @param pic2(option)
	 * @param pic3(option)
	 * @param pic4(option)
	 */
	public function updateItem(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request , 'qty' ,null,null,"integer");
		$this->checkParam($errors ,$request ,'tags',null,null,null);
		$this->checkParam($errors ,$request ,'item_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'city_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'title',null,null,null);
		$this->checkParam($errors ,$request ,'price',null,null,"integer");
		$this->checkParam($errors ,$request ,'status',null,null,"integer");
		$this->checkParam($errors ,$request ,'isDelete',null,null,"boolean");
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$item = Item::find($inputs['item_id']);

		if (!isset($item)) {
			return $this->makeWsFormat( null , 0 , '找不到要變更的商品');
		}

		//處理圖片
		for ($i=1; $i <5 ; $i++) { 
			$picName = 'pic' . $i;
			if (isset($inputs[$picName])) {
				if (!PublicUtil::startsWith($inputs[$picName] , "https://")) {
					$file = PublicUtil::picUpload($request , $picName , 600 , 600);
		   			if (isset($file)) {
			    		$pic = Constant::getS3Path().$file[$picName];
			    		$item->{$picName} = $pic;
					} else {
						return $this->makeWsFormat( null , 0 , trans('messages.imageNameUploadFail',['id',$i]));
					}
				}else{
					if ($inputs[$picName] != $item->{$picName}) {
						//return $this->makeWsFormat( null , 0 , '圖片' . $i .'上傳網址與資料庫不同');
						$item->{$picName} = $inputs[$picName];
					}
				}
				
			}else{
				//沒有上傳該圖片欄外，表示圖片被刪除
				//$item->{$picName} = null;
			}
		}
		
		//處理desc
		if (isset($inputs['desc'])) {
			$item->desc = $inputs['desc'];
		}else{
			$item->desc = Constant::getDefaultDesc();
		}

		$item->tags = $inputs['tags'];

		$item->qty = $inputs['qty'];

		$item->city_id = $inputs['city_id'];

		$item->title = $inputs['title'];

		$item->price = $inputs['price'];

		$item->enabled = $inputs['isDelete'] == "true" ? 0 : 1;

		$item->status = $inputs['status'];

		$item->save();
		
		PublicUtil::delFile('uploads');
		
		return $this->makeWsFormat($item, 1, null);
	}

	/*
	 * fun: item/getData
	 * @param check
	 * @param item_id
	 * @param current_user_id
	 * @return Dictionary []
	 */
	public function getItemData(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'item_id',null,null,"integer");
		//$this->checkParam($errors ,$request ,'current_user_id',null,null,"integer");

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();
	
		$item = Item::where('id', $inputs['item_id'])->first();
		//dd($item);
		if (!isset($item)) {
			return $this->makeWsFormat( null, 0, '找不到該商品資料');
		}
		$tracks = Track::where('item_id', $inputs['item_id'])->get();
		$comments = Comment::where('item_id', $inputs['item_id'])->get();
		
		$city = City::where('id', $item['city_id'])->first();
		if (!isset($city)) {
			return $this->makeWsFormat( null, 0, "找不到該城市資料");
		}
		
		$user = User::where('id', $item['user_id'])->first();
		if (!isset($user)) {
			return $this->makeWsFormat( null, 0, "找不到該使用者資料");
		}

	if(isset($inputs['current_user_id'])){//若沒登入了話
		$track = Track::where('user_id', $inputs['current_user_id'])->where('item_id', $inputs['item_id'])->first();
	}
	else{
		$track = null;//若沒登入了話
	}
			
		$item['available_at'] = $item['created_at']->diffForHumans();
		$item['track_qty'] = count($tracks);
		$item['comment_qty'] = count($comments);
		$item['city'] = $city['title'];
		$item['city_id'] = $city['id'];
		$item['zone_name'] = $city->zone->title;
		$item['zone_id'] = $city->zone->id;
		$item['user_name'] = $user->name;
		$item['user_pic'] = $user->pic;
		$item['user_join_at'] = $user['created_at']->diffForHumans();
		$item['isTrack'] = $track == null ? false : true;

		//處理tags欄位資料
		if (isset($item['tags'])) {
			$str_tags = substr($item['tags'] , 1 , count($item['tags'])-2);
			$tag_ary = explode(',' , $str_tags);
			for ($i=0; $i < count($tag_ary) ; $i++) { 
				$tag = Tag::find($tag_ary[$i]);
				$tag_ary[$i] = $tag->title;
			}
			$item['tagAry'] = $tag_ary;
		}
		
		$item['value_good'] = BI::queryEvaluate($user->id, '1');
		$item['value_normal'] = BI::queryEvaluate($user->id, '2');
		$item['value_bad'] = BI::queryEvaluate($user->id, '3');
	if(isset($inputs['current_user_id'])){//若沒登入了話
		//處理mode，0為未出價，1為已出價，2為使用者自己的商品	
		if ($item->user_id == $inputs['current_user_id']) {
			$mode = 2;
		}else{
			$bid = BI::queryLastBid( $inputs['item_id'] , $inputs['current_user_id']);
			if (isset($bid)) {
				$mode = 1;
			}else{
				$mode = 0;
			}
		}
	}
	else{
		$mode = 0;
	}
		$biders = BI::queryItemBiders($inputs['item_id']);

		$item['mode'] = $mode;
		if (isset($bid)) {
			$item['bid'] = $bid;
		}
		if (isset($biders)) {
			$item['biders'] = $biders;
		}
	if(isset($inputs['current_user_id'])){//若沒登入了話
		$lastBid = BI::queryLastBid($inputs['item_id'] , $inputs['current_user_id']);
	}
		if(isset($lastBid)){
			$item['lastBidPrice'] = intval($lastBid);
		}
		
		return $this->makeWsFormat($item, 1, null);
	}

	/*
	 * fun: fans/getList
	 * @param check
	 * @param user_id
	 * @param queryUser_id
	 * @param mode
	 * @return Dictionary [ 'zone_id' , 'title' , 'sort' ]
	 */
	public function getFansList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'queryUser_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'mode',null,[0,1],null);

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();
		$fans = array();
		if ($inputs['mode'] == 0) {
			//粉絲列表
			$key = 'fan_id';
			$follows = Follow::where('user_id', $inputs['queryUser_id'])->get();
		} else {
			//追蹤中
			$key = 'user_id';
			$follows = Follow::where('fan_id', $inputs['queryUser_id'])->get();
		}
		for ($i = 0; $i < count($follows); $i++) {

			$user = User::where('id', $follows[$i][$key])->first();
			if (!isset($user)) {
				return $this->makeWsFormat(null, 0, '找不到使用者流水號'.$inputs['queryUser_id'].'之資料');
			}
			$fans[$i]['id'] = $user['id'];
			$fans[$i]['desc'] = $user['desc'];
			//$fans[$i]['username'] = $user['username'];
			$fans[$i]['pic'] = $user['pic'];
			$fans[$i]['name'] = $user['name'];
			$follow = Follow::where("fan_id", $inputs['user_id'])->where("user_id", $user['id'])->first();

			$fans[$i]['isTrack'] = $follow == null ? false : true;
			$fans[$i]['sort'] = Constant::getDefaultUserSort();
		}

		if ( count($fans) == 0) {
			return $this->makeWsFormat(['fans'=>[]], 1, null);
		}else{
			return $this->makeWsFormat(['fans'=>$fans], 1, null);
		}
		
	}

	/*
	 * fun: fans/upload
	 * @param check
	 * @param user_id
	 * @param followUser_id
	 * @param isFollow
	 */
	public function uploadFans(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'followUser_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'isFollow',null,null,'boolean');

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$user = User::find($inputs['user_id']);
		$followUser = User::find($inputs['followUser_id']);
		if (!isset($user) || !isset($followUser)) {
			return $this->makeWsFormat(null, 0 , "欲變更用戶不存在");
		}

		$oldResult = Follow::where('user_id',$inputs['followUser_id'])->where('fan_id', $inputs['user_id'])->first();
		if ($inputs['isFollow'] == 'true') {
			if (!isset($oldResult)) {
				Follow::create(['user_id'=>$inputs['followUser_id'], 'fan_id'=>$inputs['user_id']]);
			}
		} else{
			if (isset($oldResult)) {
				$oldResult->delete();
			}
		}

		return $this->makeWsFormat(null, 1, null);
	}

	/*
	 * fun: like/upload
	 * @param check
	 * @param user_id
	 * @param item_id
	 * @param isLike
	 * @return Dictionary []
	 */
	public function uploadLike(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'item_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'isLike',null,null,'boolean');

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$oldResult = Track::where('user_id', $inputs['user_id'])->where('item_id', $inputs['item_id'])->first();
		if ($inputs['isLike'] == 'true') {
			if (!isset($oldResult)) {
				//dd('新增喜愛紀錄');
				Track::create(['user_id'=>$inputs['user_id'], 'item_id'=>$inputs['item_id']]);
			}
		}else{
			if (isset($oldResult)) {
				$oldResult->delete();
			}
		}

		return $this->makeWsFormat("", 1, null);
	}

	/*
	 * fun: like/userList
	 * @param check
	 * @param item_id
	 * @param current_user_id
	 * @return Dictionary likers [ 'user_id' , 'name' , 'isTrack' ,'pic' ,'sort' ]
	 */
	public function getLikerList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'item_id',null,null,"integer");

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();
		$likers = array();
		$tracks = Track::where('item_id', $inputs['item_id'])->get();
		
		for ($i = 0; $i < count($tracks); $i++) {

			$user = User::where('id', $tracks[$i]->user_id)->first();
			if (!isset($user)) {
				return $this->makeWsFormat(null, 0, '找不到使用者流水號'.$tracks[$i]->user_id.'之資料');
			}
			$likers[$i]['id'] = $user['id'];
			//$fans[$i]['username'] = $user['username'];
			$likers[$i]['pic'] = $user['pic'];
			$likers[$i]['name'] = $user['name'];
			$likers[$i]['desc'] = $user['desc'];
			$follow = Follow::where("fan_id", $inputs['current_user_id'])->where("user_id", $user->id)->first();

			$likers[$i]['isTrack'] = $follow == null ? false : true;
			$likers[$i]['sort'] = Constant::getDefaultUserSort();
		}

		if ( count($likers) == 0) {
			return $this->makeWsFormat(['likers'=>[]], 1, null);
		}else{
			return $this->makeWsFormat(['likers'=>$likers], 1, null);
		}
		
	}

	/*
	 * fun: comment/getList
	 * @param check
	 * @param item_id
	 * @return Dictionary [ 'item_id' , 'content' , 'user_id', 'username', 'username', 'commit_at' ]
	 */
	public function getCommentList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'item_id',null,null,"integer");

		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$item = Item::find($inputs['item_id']);
		if (!isset($item)) {
			return $this->makeWsFormat( null, 0, "所提供商品並不存在");
		}

		$comments = Comment::where('item_id' , $inputs['item_id'])->get();

		for ($i = 0; $i < count($comments); $i++) {
			//得到的不應該是username
			$username = User::where('id', $comments[$i]['user_id'])->first();
			$comments[$i]['username'] = $username->name;
			$comments[$i]['userPic'] = $username->pic;
			$comments[$i]['commit_at'] = $comments[$i]['created_at']->diffForHumans();
		}
		return $this->makeWsFormat(['comments'=>$comments], 1, null);
	}

	/*
	 * fun: comment/upload
	 * @param check
	 * @param item_id
	 * @param user_id
	 * @param content
	 * @return Dictionary [ 'item_id' , 'content' , 'user_id', 'username', 'username', 'commit_at' ]
	 */
	public function uploadComment(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'item_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'content',null,null,null);
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$user = User::find($inputs['user_id']);
		if (!isset($user)) {
			return $this->makeWsFormat( null, 0, "所提供用戶並不存在");
		}

		$item = Item::find($inputs['item_id']);
		if (!isset($item)) {
			return $this->makeWsFormat( null, 0, "所提供商品並不存在");
		}

		$newComment = Comment::create(['item_id'=>$inputs['item_id'], 'user_id'=>$inputs['user_id'], 'content'=>$inputs['content']]);

		return $this->makeWsFormat(['item_id'=>$newComment->item_id, 'user_id'=>$newComment->user_id, 'content'=>$newComment->content], 1, null);
	}

	/*
	 * fun: rating/getList
	 * @param check
	 * @param user_id
	 * @return Dictionary []
	 */
	public function getRatingList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		$user = User::find($inputs['user_id']);
		if (!isset($user)) {
			return $this->makeWsFormat( null, 0, "所提供用戶並不存在");
		}

		$showRatings = [];

		$evaluates = Evaluate::where('receiver_id',$inputs['user_id'])->orderBy('created_at','desc')->get();
		if(count($evaluates) == 0){
			return $this->makeWsFormat(['ratings'=>[]], 1, null);
		}
		$index = 0;
		for ($i = 0; $i < count($evaluates); $i++) {
			$evaluate = $evaluates[$i];
			//$ratingUser = User::find($evaluate['giver_id']);
			$showRatings[$index]['item_id'] = $evaluate['item_id'];
			$item_pic = Item::find($evaluate['item_id']);
			$showRatings[$index]['item_pic'] = $item_pic['pic1'];
			$showRatings[$index]['role'] = $evaluate['role'];
			$showRatings[$index]['type'] = $evaluate['type'];
			$showRatings[$index]['content'] = $evaluate['content'];
			$ratingUser = User::find($evaluate['giver_id']);
			$showRatings[$index]['fromUser_id'] = $evaluate['giver_id'];
			$showRatings[$index]['fromUser_name'] = $ratingUser['name'];
			$showRatings[$index]['fromUser_pic'] = $ratingUser['pic'];
			$showRatings[$index]['toUser_id'] = $evaluate['receiver_id'];
			$showRatings[$index]['rating_vocal_at'] = $evaluate['created_at']->diffForHumans();
			$showRatings[$index]['rating_at'] = $evaluate['created_at']->format('m/d/Y');
			$showRatings[$index]['mode'] = 1;

			//處理回覆評價
			// $reply_evaluate = Evaluate::where('item_id',$evaluate->item_id)->where('giver_id',$inputs['user_id'])->where('role', '<>', $evaluate->role)->first();
			// if(isset($reply_evaluate)){
			// 	$index ++;
			// 	$showRatings[$index]['item_id'] = $reply_evaluate['item_id'];
			// 	$showRatings[$index]['role'] = $reply_evaluate['role'];
			// 	$showRatings[$index]['type'] = $reply_evaluate['type'];
			// 	$showRatings[$index]['content'] = $reply_evaluate['content'];
			// 	$ratingUser = User::find($reply_evaluate['giver_id']);
			// 	$showRatings[$index]['fromUser_id'] = $reply_evaluate['giver_id'];
			// 	$showRatings[$index]['fromUser_name'] = $ratingUser['name'];
			// 	$showRatings[$index]['fromUser_pic'] = $ratingUser['pic'];
			// 	$showRatings[$index]['toUser_id'] = $reply_evaluate['receiver_id'];
			// 	$showRatings[$index]['rating_vocal_at'] = $reply_evaluate['created_at']->diffForHumans();
			// 	$showRatings[$index]['rating_at'] = $reply_evaluate['created_at']->format('m/d/Y');
			// 	$showRatings[$index]['mode'] = 2;
			// }
			$index ++;
		}

		return $this->makeWsFormat(['ratings'=>$showRatings], 1, null);
	}

	/* 暫時設定每人針對某商品只能就同一角色給一次評價
	 * fun: rating/upload
	 * @param check
	 * @param item_id
	 * @param fromUser_id
	 * @param fromUser_role
	 * @param toUser_id
	 * @param content
	 * @param type
	 * @return Dictionary []
	 */
	public function uploadRating(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'item_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'fromUser_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'fromUser_role',null,[1,2],null);
		$this->checkParam($errors ,$request ,'toUser_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'content',300,null,null);
		$this->checkParam($errors ,$request ,'type',null,[1,2,3],null);
		if (count($errors) > 0) {
			return $errors[0];
		}

		$inputs = $request->all();

		$user = User::find($inputs['fromUser_id']);
		if (!isset($user)) {
			return $this->makeWsFormat(null , 0 , '所輸入用戶不存在');
		}

		$user_receiver = User::find($inputs['toUser_id']);
		if (!isset($user_receiver)){
			return $this->makeWsFormat(null , 0 , '所輸入用戶不存在');
		}

		$item = Item::find($inputs['item_id']);
		if (!isset($item)) {
			return $this->makeWsFormat(null , 0 , '所輸入商品不存在');
		}

		//檢查是否有賣家或買家對自己給評價的狀況
		if ($inputs['fromUser_role'] == 1) {
			//評價者立場為供給方
			if ($item->user->id != $user->id) {
				return $this->makeWsFormat(null , 0 , '所評價商品非評價人所有');
			}
		}else{
			//評價者立場為需求方
			if ($item->user->id == $user->id) {
				return $this->makeWsFormat(null , 0 , '所評價商品為評價人所有');
			}
		}

		$oldRating = Evaluate::where('item_id',$item->id)->where('role',$inputs['fromUser_role'])->where('giver_id',$user->id)->first();
		//只有沒進行過評價的情況下才能新增評價
		if (!isset($oldRating)) {
			$rating = Evaluate::create(['item_id'=>$item->id, 'role'=>$inputs['fromUser_role'], 'giver_id'=>$user->id, 'receiver_id'=>$user_receiver->id, 'type'=>$inputs['type'], 'content'=>$inputs['content']]);
			return $this->makeWsFormat(['rating'=>$rating], 1, null);
		}else{
			return $this->makeWsFormat(null, 0, '評價人已對該商品進行過評價');
		}		
	}

	/* 此版本的訊息人物頭像永遠顯示對方的，不論是誰的訊息
	 * fun: msg/getList
	 * @param check
	 * @param current_user_id
	 * @param user_id(option)
	 * @param item_id(option)
	 * @param room_id(option)
	 * @param refresh_at(option) 為Message id的讀取時最大值
	 * @return Dictionary []
	 */
	public function getMsgList(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'current_user_id',null,null,"integer");
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();
		if (!isset($inputs['user_id']) && !isset($inputs['room_id']) && !isset($inputs['item_id'])) {
			return $this->makeWsFormat( null , 0  , '缺少parameter:user_id或room_id或item_id');
		}

		//查詢關於某位使用者的最新訊息，用於收件夾
		if (isset($inputs['user_id'])) {
			$sql = 'SELECT * FROM messages where (`room_id` , `created_at`) in (SELECT `room_id` , MAX(`created_at`) FROM messages group by `room_id` ) and `room_id` in (SELECT `id` from rooms where `user1`=' . $inputs['user_id'] . ' or `user2`=' . $inputs['user_id'] .') order by created_at desc';
			//dd($sql);
			$results = DB::select($sql);
			$msgs = Message::hydrate($results);
			$this->refineMsgs($msgs , true , $inputs['current_user_id']);
			return $this->makeWsFormat(['msgs'=>$msgs], 1, null);
		}else{
			//dd('沒有user_id');
		}

		//查詢聊天室內是否有新的訊息
		if (isset($inputs['room_id'])) {
			if(!isset($inputs['refresh_at'])){
				//為第一次進入聊天室
				$sql = 'SELECT * FROM messages where room_id = ' . $inputs['room_id'];
				//dd($sql);
				$results = DB::select($sql);
				$msgs = Message::hydrate($results);

				//如果有發給自己的訊息，於收取後全部改為已讀
				for ($i=0; $i < count($msgs) ; $i++) { 
					if($msgs[$i]->isRead == false && $msgs[$i]->toUser_id == $inputs["current_user_id"]){
						$msgs[$i]->isRead = true;
						$msgs[$i]->save();
					}	
				}

			}else{
				//為聊天室中的定時更新
				//$sql = 'SELECT * FROM messages where room_id = ' . $inputs['room_id'] . ' and `id` > ' . $inputs['refresh_at'] . ' and `fromUser_id` <> ' . $inputs['current_user_id'];
				$sql = 'SELECT * FROM messages where room_id = ' . $inputs['room_id'] . ' and `id` > ' . $inputs['refresh_at'];
				//dd($sql);
				$results = DB::select($sql);
				$msgs = Message::hydrate($results);
				//如果有發給自己的訊息，於收取後全部改為已讀
				for ($i=0; $i < count($msgs) ; $i++) { 
					if($msgs[$i]->isRead == false && $msgs[$i]->toUser_id == $inputs["current_user_id"]){
						$msgs[$i]->isRead = true;
						$msgs[$i]->save();
					}	
				}
			}
			$this->refineMsgs($msgs , false , $inputs['current_user_id']);

			//處理訊息列Mode
			$mode = BI::queryRoomMode($inputs['room_id'] , $inputs['current_user_id']);

			$result = ['msgs'=>$msgs, 'refresh_at'=>PublicUtil::queryRefresh_at(), 'mode'=>$mode];

			//處理lastBid欄位
			$room = Room::find($inputs['room_id']);
			if($room == null){
				return $this->makeWsFormat(null, 0, "所查詢房間並不存在");
			}else{
				$lastBid = BI::queryLastBid($room->item_id , null , $room->id);
				if(isset($lastBid)){
					$result["lastBid"] = intval($lastBid);
				}
			}

			//處理other_name
			if($room->user1 == $inputs['current_user_id']){
				$other_user = User::find($room->user2);
			}else{
				$other_user = User::find($room->user1);
			}

			$result['other_name'] = $other_user->name;
			
			return $this->makeWsFormat($result, 1, null);
		}
		//查詢關於某個商品的自有聊天室之所有訊息，並回傳該聊天室id
		if (isset($inputs['item_id'])) {
			$item = Item::where('id', $inputs['item_id'])->first();
			if ($item == null) {
				return $this->makeWsFormat(null, 0, "所查詢商品並不存在");
			}else{
				//查詢聊天室，條件為某商品與某買家(user2)，找不到就創一間聊天室
				$roomAry = DB::select('select * from rooms where item_id='.$inputs['item_id'].' and user2='.$inputs['current_user_id']);
				$room = null;
				if (count($roomAry) == 1) {
					$room = $roomAry[0];
				}else{
					$room = Room::create(['item_id'=>$inputs['item_id'],'user1'=>$item->user_id,'user2'=>$inputs['current_user_id']]);
				}

				$inputs['room_id'] = $room->id;
				$results = DB::select('select * from messages where `room_id` = ' . $room->id);
				$msgs = Message::hydrate($results);
				//如果有發給自己的訊息，於收取後全部改為已讀
				for ($i=0; $i < count($msgs) ; $i++) { 
					if($msgs[$i]->isRead == false && $msgs[$i]->toUser_id == $inputs["current_user_id"]){
						$msgs[$i]->isRead = true;
						$msgs[$i]->save();
					}	
				}

				$this->refineMsgs($msgs , false , $inputs['current_user_id']);

				//處理訊息列Mode
				$mode = BI::queryRoomMode($room->id , $inputs['current_user_id']);
				//return $this->makeWsFormat(['msgs'=>$msgs, 'room_id'=>$room->id, 'mode'=>$mode], 1, null);

				$result = ['msgs'=>$msgs, 'refresh_at'=>PublicUtil::queryRefresh_at(), 'room_id'=>$room->id, 'mode'=>$mode];

				//處理lastBid欄位
				$lastBid = BI::queryLastBid($inputs['item_id'] , null , $room->id);
				if(isset($lastBid)){
					$result["lastBid"] = intval($lastBid);
				}

				//處理other_name
				if($room->user1 == $inputs['current_user_id']){
					$other_user = User::find($room->user2);
				}else{
					$other_user = User::find($room->user1);
				}
				
				$result['other_name'] = $other_user->name;

				return $this->makeWsFormat($result, 1, null);
			}
		}
		return $this->makeWsFormat(null, 0, "參數不足");
	}

	/*
	 * fun: msg/upload
	 * @param check
	 * @param fromUser_id
	 * @param toUser_id
	 * @param room_id
	 * @param mode
	 * @param item_id(option) 暫不使用
	 * @param content(option)
	 * @param contentPic(option)
	 * @return msg_id
	 */
	public function uploadMsg(Request $request) {
		$isHigher = false; //判斷是否加入開價只能更高的檢查開關
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'fromUser_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'toUser_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'room_id',null,null,"integer");
		$this->checkParam($errors ,$request ,'mode',null,[0,1,2,3,4],null);
		if (count($errors) > 0) {
			return $errors[0];
		}
		$inputs = $request->all();

		if($inputs['mode'] == 0 ){
			if (!isset($inputs['content']) && !isset($inputs['contentPic']) ) {
				return $this->makeWsFormat( null , 0  , '缺少parameter:content或contentPic');
			}
		}

		if($inputs['mode'] == 1 ){
			if (!isset($inputs['bid']) ) {
				return $this->makeWsFormat( null , 0  , '缺少parameter:bid');
			}
		}
		
		
		// if(isset($inputs['item_id'])){
		// 	$item = Item::find($inputs['item_id']);
		// 	if (!isset($item)) {
		// 		return $this->makeWsFormat( null , 0  , '所輸入商品不存在');
		// 	}
		// }
		
		$fromUser = User::find($inputs['fromUser_id']);
		$toUser = User::find($inputs['toUser_id']);
		if (!isset($fromUser) || !isset($toUser)) {
			return $this->makeWsFormat( null , 0  , '所輸入用戶不存在');
		}
		$room = Room::find($inputs['room_id']);
		if (!isset($room)) {
			return $this->makeWsFormat( null , 0  , '所輸入房間不存在');
		}else{
			$item = $room->item;
		}

		//判斷mode的狀態是否為1(出價)
		// if ($inputs['mode'] == 1 ) {
		// 	//
		// 	if ($inputs['bid'] < $item->price) {
		// 		return $this->makeWsFormat( null , 0 , trans('messages.mustHigherPrice',['price'=>$item->price]));
		// 	}
		// 	if ($isHigher) {
		// 		//開價必須比最新的出價來得高
		// 		$bid = BI::queryLastBid( $item->id , $fromUser->id);
		// 		if ($bid != null && $inputs['bid'] <= $bid) {
		// 			return $this->makeWsFormat( null , 0 , trans('messages.mustHigherBid',['bid'=>$bid]));
		// 		}
		// 	}
		// }
		

		//處理contentPic
		if (isset($inputs['contentPic'])) {
			list($width, $height) = getimagesize(Input::file('contentPic'));
			//dd('width:' . $width . ',height:' . $height);
			$ratio = $width / 400 ; //設定寬度為400
			$file = PublicUtil::picUpload($request , 'contentPic' );
		    if (isset($file)) {
		    	//dd($file);
			    $pic = Constant::getS3Path().$file['contentPic'];
			    $inputs['content'] = $pic;
			} else {
				return $this->makeWsFormat( null , 0 , '訊息圖片上傳失敗');
			}
		}

		if(!isset($inputs['item_id'])){
			$inputs['item_id'] = $room->item_id;
		}

		$msg = Message::create($inputs);

		if (isset($msg)) {
			//如果mode為3，將item狀態改為售出1
			if ($inputs['mode'] == 3) {
				$item->status = 1;
				$item->save();
			}
			return $this->makeWsFormat(['msg_id'=>$msg->id], 1, null);
		}else{
			return $this->makeWsFormat(null, 0, trans('messages.messageUploadFail'));
		}
	}

	/* 本版本設定只有別人寫給自己的訊息才算是新訊息
	 * fun: msg/getNewLetters
	 * @param check
	 * @param user_id
	 * @return Dictionary [newLetters]
	 */
	public function getNewLettersCount(Request $request) {
		$errors = array();
		$this->basicCheck($errors ,$request);
		$this->checkParam($errors ,$request ,'user_id',null,null,"integer");
		if (count($errors) > 0) {
			return $errors[0];
		}

		$inputs = $request->all();

		$result = [];
		$results = DB::select('SELECT count(`item_id`) as newLetters FROM (SELECT * FROM messages where (`toUser_id`='.$inputs['user_id'].') and `isRead` = 0 GROUP BY `item_id`) AS a');
		//$results = DB::select('SELECT count(`item_id`) as newLetters FROM (SELECT * FROM messages where (`fromUser_id`='.$inputs['user_id'].' or `toUser_id`='.$inputs['user_id'].') and `isRead` = 0 GROUP BY `item_id`) AS a');
		$result['newLetters'] = intval($results[0]->newLetters);
		return $this->makeWsFormat($result, 1, null);
	}

	//以下為WS Library============================

	/*
		為$msgs加入前台所必要的其他資訊
		@param $advance 是否追加更多資訊，收件夾頁面適用
		@param $currentUser_id 登入者流水號
	*/
	private function refineMsgs($msgs ,$advance ,$currentUser_id)
	{
		for ($i = 0; $i < count($msgs); $i++) {
			$item = Item::where('id', $msgs[$i]->item_id)->first();
			//dd($item);
			$fromUser = User::where('id', $msgs[$i]->fromUser_id)->first();
			$toUser = User::where('id', $msgs[$i]->toUser_id)->first();

			$msgs[$i]->itemOwner_id = $item['user_id'];
			$msgs[$i]->itemOwner_name = $item->user->name;
			$msgs[$i]->item_name = $item['title'];
			$msgs[$i]->item_pic = $item['pic1'];
			$msgs[$i]->fromUser_name = $fromUser['name'];

			//處理other_name
			if($fromUser->id == $currentUser_id){
				$msgs[$i]->other_name = $toUser->name;
			}else{
				$msgs[$i]->other_name = $fromUser->name;
			}

			$created_at = $msgs[$i]->created_at;
			$created_at_date = Carbon::createFromFormat('Y-m-d H:i:s', $created_at);
			$msgs[$i]->msg_vocal_at = $created_at_date ->diffForHumans();
			$msgs[$i]->msg_at = $created_at_date ->format('Y/m/d H:i');

			if($advance){
				$msgs[$i]->item_price = $item['price'];
				$track_qty = Track::where('item_id', $msgs[$i]->item_id)->get();
				$msgs[$i]->track_qty = count($track_qty);
				$comment_qty = Comment::where('item_id', $msgs[$i]->item_id)->get();
				$msgs[$i]->comment_qty = count($comment_qty);
				$msgs[$i]->fromUser_pic = $fromUser['pic'];
				if($fromUser->id != $currentUser_id ){
					$msgs[$i]->userPic = $fromUser['pic'];
				}else{
					$msgs[$i]->userPic = $toUser['pic'];
				}
			}
		}
	}

	/*
	 * 檢查是否有提供check和deviceCode欄位資料，驗證正確就回傳null，否則回傳makeWsFormat
	 * @param errors 錯誤字串陣列
	 * @param $request 請求物件
	 * @return makeWsFormat or null
	 */
	private function basicCheck(&$errors , Request $request )
	{
		$inputs = $request->all();
		$path = $request->path();
		//裝置碼檢查
		if (!isset($inputs['deviceCode'])) {
			$errors[] = $this->makeWsFormat( null , 0  , '缺少parameter:deviceCode');
		}else{
			//除了device/token和device/register，其它需有註冊device才能使用
			if (!PublicUtil::startsWith($path,'service/device/')) {
				$device = Device::where('deviceCode' , $inputs['deviceCode'])->first();
				if (!isset($device)) {
				 	$errors[] = $this->makeWsFormat( null , 0  , trans('messages.deviceUnregistered'));
				 }
			} 
		}

		//當環境為開發環境時，不進行驗證碼檢查
		if (App::environment('local') || App::environment('dev') || App::environment('testing')) {
    		return null;	
		}else{
			if (!isset($inputs['check'])) {
				$errors[] = $this->makeWsFormat( null , 0  , '缺少parameter:check');
			}else{
				$result = true;
				//dd('inputCheck:'. $inputs['check']);
				if ($path != 'service/device/register') {
					$fun = substr($path , 8);
				 	$salt = $device->salt;
					if ($inputs['check'] != BI::buildCheck($salt ,$fun)) {
						$result = false;
					}
				}else{
					if ($inputs['check'] != BI::buildCheck($inputs['token'],$inputs['deviceCode'])) {
						$result = false;
					}
				}
				if ($result == false) {
					//dd('驗證碼錯誤');
					$errors[] = $this->makeWsFormat( null , 0  , trans('messages.chkWrong'));
				}
			}
		}
	}

	//檢查參數是否存在,$errors為錯誤字串陣列,$request為請求物件,$param為參數名稱,$length為最大長度,$values為選項陣列
	private function checkParam(&$errors , Request $request , $param , $length = null , $values = null , $type = null){
		$inputs = $request->all();
		//檢查是否有值
		if (!isset($inputs[$param]) || strlen(trim($inputs[$param])) == 0) {
			$errors[] = $this->makeWsFormat( null , 0  , '缺少parameter:' . $param);
		}else{
			//檢查長度是否符合
			if ($length != null) {
				$validator = Validator::make($inputs, [ $param => 'max:' . $length]);
		        if ($validator->fails()) {
		            $errors[] = $this->makeWsFormat(null, 0 , trans('messages.nameTooLong',['length'=>$length]));
		        }
			}
			
			//檢查值是否為指定選項內容之一
			if ($values != null && count($values) > 0){
				$result = false;
				for ($i=0; $i < count($values); $i++) { 
					if ($inputs[$param] == $values[$i]) {
						$result = true;
					}
				}
				if ($result == false) {
					$errors[] = $this->makeWsFormat( null , 0  , $param . '格式錯誤');
				}
			}

			//檢查值是否為特定類型Type
			if ($type != null) {
				if ($type == 'boolean') {
					$value = $inputs[$param];
					if ($value != 'true' && $value != 'false') {
						$errors[] = $this->makeWsFormat( null , 0  , $param . '非boolean格式');
					}
				}else{
					$validator = Validator::make($inputs, [$param=>$type]);
			        if ($validator->fails()) {
			            $errors[] = $this->makeWsFormat(null, 0 ,  $param .'格式錯誤');
			        }
				}
				
			}
		}
	}

	/*
	 * 製作符合WS API格式的JSON字串
	 * @param $content 查詢結果
	 * @param $RetCode  查詢是否成功
	 * @param $msg     查詢錯誤訊息
	 * @return JSON字串
	 */
	private function makeWsFormat( $content , $status , $msg){
		$header = ['Content-type'=> 'application/json; charset=utf-8'];

		if ($status  == 1) {
			return Response::json(array('RetCode'=> 1 , 'Content'=>$content) , 200 ,$header , JSON_UNESCAPED_UNICODE);
		}else{
			return Response::json(array('RetCode'=> 0, 'Msg'=>$msg) , 200 , $header , JSON_UNESCAPED_UNICODE);
		}
	}

	public function findname(Request $request) {

		$users = User::orderBy('created_at','asc')->get();
		if (count($users) != 0) 
		{
		
			for ($i=44; $i <=47 ; $i++) 
			{ 	
				$user = User::where( 'id' , $i)->first();
				//dd($user);
				$inputs['name'] = $user['username'];
				$user->update($inputs);
				//$user[$i]['name'] = $users[$i]['name'];
			}
		}	
		else
		{
			$empty = [];
			$users = $empty;
		}

		//return $this->makeWsFormat($users,1,null);

	}

}