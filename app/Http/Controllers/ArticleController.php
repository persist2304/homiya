<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Article;
use App\Http\Requests\ArticleRequest;
use Request as urlRequest;
use App\Http\Model\PublicUtil;
use App\Http\Model\Constant;

use DOMDocument;
use DOMXPath;
use DB;
class ArticleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }

	public function index()
	{
		$allArticle = Article::all();
		return view ('articles.index',compact('allArticle'));
	}
	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$urlNow = urlRequest::url();
		return view ('articles.create',compact('urlNow'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ArticleRequest $request)
	{
		$articles_inputs = $request->all();
		
		$articles_store = Article::create($articles_inputs);

		

		return redirect('/articles');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$articles_show = Article::findOrFail($id);
		return view ('articles.show',compact('articles_show'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$urlNow = urlRequest::url();
		$articles_edit = Article::findOrFail($id);
		return view('articles.edit', compact('articles_edit','urlNow'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,ArticleRequest $request)
	{
		$articles = Article::findOrFail($id);
		$article_inputs = $request->all();
		$file = PublicUtil::picUpload($request , 'content',420,600);
		if (isset($file['content'])) 
		{
			$article_inputs['content'] = Constant::getS3Path().$file['content'];	
			//dd($article_inputs);
			$articles->update($article_inputs);
			return redirect('/articles');
		}
		else
		{
			//dd($article_inputs);
			$articles->update($article_inputs);
			return redirect('/articles');
		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	

}
