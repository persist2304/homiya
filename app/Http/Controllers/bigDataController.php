<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Item;
use App\Banner;




class bigDataController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//計算目前所有店家數
		$user_qty = User::all()->count();
		//計算目前所有商品數
		$item_qty = Item::all()->count();
		//計算目前所有成交數
		$deal_qty = Item::where('status','=',1)->get()->count();
		//計算目前所有廣告點擊數
		$clicks_qty = 0;
		$banners = Banner::all();
		foreach ($banners as $banner) 
  		{
  			$clicks_qty = $clicks_qty + $banner->clicks;
  		}
		return view ('bigDatas.index',compact('user_qty','item_qty','deal_qty','clicks_qty'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
