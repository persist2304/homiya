<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use View;
use Auth;
use Hash;
use App\Admin;
use Illuminate\Support\Facades\Redirect;




class MyAuthController extends Controller 
{
	public function show()
	{
		return View::make('login');
	}

	public function login(Request $request)
	{
		//驗證帳號與密碼是否均已填寫
		$this->validate($request , ['username' => 'required' , 'password' => 'required' ]);  
		$inputs = $request->all();
		//根據帳號去尋找是否有符合的資料
		$theAdmin = Admin::where( 'username', $inputs['username'])->first();

		if (isset($theAdmin)) 
		{
			//比對密碼欄是否與符合的資料相同，因為密碼都有經過Hash加密，故使用Hash::check進行比對，第一參數為Hash碼，第二參數為比對字串，不可對調
			if (Hash::check($inputs['password'] , $theAdmin->password )) 
			{  
				//Hash:check(Hash碼，要比對的字串)
				//以符合資料的帳號身份進行登入
				Auth::loginUsingId($theAdmin->id);
				return Redirect::intended('users');
			}
		}
		flash()->error('帳號或密碼錯誤');
		return Redirect::to('login');
	}
	public function logout()
	{
		Auth::logout();
		return Redirect::to('login');
	}

}
