<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*自我生成*/
Route::get('/','UsersController@index');
Route::resource('users','UsersController');
Route::resource('banners','BannerController');
Route::resource('tags','TagsController');
Route::resource('items','ItemsController');
Route::resource('items_down','ItemsDownController');
Route::resource('bigDatas','bigDataController');
Route::resource('articles','ArticleController');
Route::any('mobileRegister', 'MobileController@postRegister');
Route::any('mobileLogin', 'MobileController@postLogin');
Route::get('login','MyAuthController@show');
Route::post('login','MyAuthController@login');
Route::get('logout','MyAuthController@logout');
Route::any('service/device/token', 'WebServiceController@getToken');
Route::any('service/device/register', 'WebServiceController@registerDevice');
Route::any('service/user/normalLogin', 'WebServiceController@normalLogin');
Route::any('service/user/register', 'WebServiceController@normalRegister');
Route::any('service/user/fbLogin', 'WebServiceController@fbLogin');
Route::any('service/user/autoLogin', 'WebServiceController@autoLogin');
Route::any('service/user/changePwd', 'WebServiceController@changePwd');
Route::any('service/user/getData', 'WebServiceController@getUserData');
Route::any('service/user/update', 'WebServiceController@updateUserData');
Route::any('service/country/getList', 'WebServiceController@getCountryList');
Route::any('service/zone/getList', 'WebServiceController@getZoneList');
Route::any('service/city/getList', 'WebServiceController@getCityList');
Route::any('service/item/getList', 'WebServiceController@getItemList');
Route::any('service/item/getData', 'WebServiceController@getItemData');
Route::any('service/item/upload', 'WebServiceController@uploadItem');
Route::any('service/item/update', 'WebServiceController@updateItem');
Route::any('service/comment/getList', 'WebServiceController@getCommentList');
Route::any('service/comment/upload', 'WebServiceController@uploadComment');
Route::any('service/banner/getList', 'WebServiceController@getBannerList');
Route::any('service/banner/click', 'WebServiceController@clickBanner');
Route::any('service/tag/getList', 'WebServiceController@getTagList');
Route::any('service/fans/getList', 'WebServiceController@getFansList');
Route::any('service/fans/upload', 'WebServiceController@uploadFans');
Route::any('service/like/upload', 'WebServiceController@uploadLike');
Route::any('service/like/userList', 'WebServiceController@getLikerList');
Route::any('service/rating/getList', 'WebServiceController@getRatingList');
Route::any('service/rating/upload', 'WebServiceController@uploadRating');
Route::any('service/msg/getList', 'WebServiceController@getMsgList');
Route::any('service/msg/upload', 'WebServiceController@uploadMsg');
Route::any('service/msg/getNewLetters', 'WebServiceController@getNewLettersCount');

// Route::any('service/help/findname', 'WebServiceController@findname');
//重設密碼
// Route::get('/password/email','Auth\PasswordController@getEmail');
Route::get('password/email','UserPasswordRestController@getEmail');
// Route::get('homiya/public/password/email','UserPasswordRestController@getEmail');
// Route::any('/password/email','Auth\PasswordController@postEmail');
Route::post('password/email','UserPasswordRestController@postEmail');
// Route::post('homiya/public/password/email','UserPasswordRestController@postEmail');
// Route::get('/password/reset/{token}','Auth\PasswordController@getReset');
Route::get('password/reset',['as'=>'resetPwd','uses'=>'UserPasswordRestController@getReset']);
// Route::post('/password/reset','Auth\PasswordController@postReset');
Route::post('password/reset','UserPasswordRestController@postReset');
Route::get('/countSuccessful','ItemsController@index');

Route::get('close', function(){
	return View::make('close');
});
//測試頁面
Route::get('wsTest', 'WebServiceController@show');

/*自我生成*/
/*
以下範例為tutorial範例，正式上線需刪除
*/
Route::get('/tutorial', function()
{
	return View::make('tutorialHome');
});

Route::get('/charts', function()
{
	return View::make('mcharts');
});

Route::get('/tables', function()
{
	return View::make('table');
});

Route::get('/forms', function()
{
	return View::make('form');
});

Route::get('/grid', function()
{
	return View::make('grid');
});

Route::get('/buttons', function()
{
	return View::make('buttons');
});


Route::get('/icons', function()
{
	return View::make('icons');
});

Route::get('/panels', function()
{
	return View::make('panel');
});

Route::get('/typography', function()
{
	return View::make('typography');
});

Route::get('/notifications', function()
{
	return View::make('notifications');
});

Route::get('/blank', function()
{
	return View::make('blank');
});

Route::get('/login', function()
{
	return View::make('login');
});

Route::get('/documentation', function()
{
	return View::make('documentation');
});