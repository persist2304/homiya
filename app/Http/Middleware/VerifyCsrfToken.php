<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ('testing' !== app()->environment())
        {
            //dd(app()->environment());
            $skips = [
                'mobileLogin',
                'mobileRegister',
                'password/reset',
                'password/email',
                'close',
                'service/device/token',
                'service/device/register',
                'service/user/fbLogin',
                'service/user/normalLogin',
                'service/user/autoLogin',
                'service/user/register',
                'service/user/changePwd',
                'service/user/getData',
                'service/user/update',
                'service/comment/upload',
                'service/comment/getList',
                'service/zone/getList',
                'service/city/getList',
                'service/country/getList',
                'service/item/getList',
                'service/item/getData',
                'service/item/upload',
                'service/item/update',
                'service/like/upload',
                'service/like/userList',
                'service/fans/getList',
                'service/fans/upload',
                'service/tag/getList',
                'service/rating/getList',
                'service/rating/upload',
                'service/banner/getList',
                'service/msg/getList',
                'service/msg/getNewLetters',
                'service/msg/upload',
                'service/banner/click',

            ];
            foreach ($skips as $route) {
                if ($request->is($route)) {
                    return $this->addCookieToResponse($request, $next($request));
                }
            }
            return parent::handle($request, $next);
        }
        return $next($request);
    }

}
