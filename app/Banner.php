<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model 
{

	protected $table = 'banners';

	protected $fillable = ['title','pic','mode','url','target','clicks','sort','enabled'];

	protected $casts = [
		'id' =>'integer',
		'mode' => 'integer',
		'clicks' => 'integer',
		'sort' => 'integer',
		'enabled' => 'integer',
	];
}