<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;


class Admin extends Model implements AuthenticatableContract
{
	use Authenticatable;

	protected $table = 'admins';

	protected $fillable = ['username','password','role'];

	public $timestamps = false;

	protected $hidden = ['remember_token'];
}