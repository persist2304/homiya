<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model 
{
	public $timestamps = false;

	protected $table = 'tracks';

	protected $fillable = ['item_id','user_id'];

	protected $casts = [
		'id' => 'integer',
		'item_id' => 'integer',
		'user_id' => 'integer',
	];
}