<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model 

{
	protected $table = 'cities';

	protected $fillable = ['latitude','longitude','title','zone_id','sort','enabled'];

	protected $casts = [
		'id' => 'integer',
		'latitude' => 'double',
		'longitude' => 'double',
		'zone_id' => 'integer',
		'sort' => 'integer',
		'enabled' => 'integer',
	];

	public $timestamps = false;

	//每個城市只屬於某個區域
	public function zone()
	{
		return $this->belongsTo('App\Zone');
	}
}